/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney;

import eapli.mymoney.application.RegisterInitialBalanceController;
import eapli.mymoney.bootstrap.Bootstraper;
import eapli.mymoney.presentation.MainMenu;
import eapli.mymoney.presentation.RegisterInitialBalanceUI;
import eapli.util.Console;

/**
 *
 * @author Paulo Gandra Sousa
 */
public final class MyMoney {

    private MyMoney() {
        // to make sure this is an utility class
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final Bootstraper bootstraper = new Bootstraper();
        bootstraper.execute();

        if (canDisplayOptions()) {
            final MainMenu menu = new MainMenu();
            menu.mainLoop();
        }

    }

    // TODO estes dois métodos deveriam estar noutra classe para que esta respeitasse o Single Responsibility Principle
    private static boolean canDisplayOptions() {
        final RegisterInitialBalanceController theInitialBalanceController = new RegisterInitialBalanceController();
        if (theInitialBalanceController.existsInitialBalance() == false) {
            return askForInitialBalance(theInitialBalanceController);
        }
        return true;
    }

    // TODO estes dois métodos deveriam estar noutra classe para que esta respeitasse o Single Responsibility Principle
    private static boolean askForInitialBalance(
            RegisterInitialBalanceController theInitialBalanceController) {
        int option = -1;
        while (option != 0) {
            System.out.
                    println("=========================================================");
            System.out.
                    println("Before using MyMoney you need to input the Initial Balance");
            System.out.println("1. Register Initial Balance");
            System.out.println("0. Exit\n\n");

            option = Console.readInteger("Please choose an option");
            switch (option) {
                case 0:
                    System.out.println("bye bye ...");
                    return false;
                case 1:
                    final RegisterInitialBalanceUI theInitialBalanceUI = new RegisterInitialBalanceUI();
                    theInitialBalanceUI.show();
                    if (theInitialBalanceController.existsInitialBalance()) {
                        return true;
                    }
                    break;

                default:
                    System.out.println("option not recognized.");
                    break;
            }
        }
        return true;
    }
}
