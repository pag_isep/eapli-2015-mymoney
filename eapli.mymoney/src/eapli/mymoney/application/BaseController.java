/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.Controller;
import eapli.framework.domain.Money;
import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.domain.movements.ExpenseRecord;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.ExpenseRepository;
import eapli.mymoney.persistence.Persistence;
import eapli.util.DateTime;
import java.util.Date;

/**
 * All Controller must extend this class
 *
 */
public abstract class BaseController implements Controller {

    /**
     * gets the expenditure of the current week.
     *
     * this method follows an "OO approach" by asking the repository to recreate
     * the expense record of the desired week and performs the calculation in
     * memory. doing the calculation in memory might not be as performant as
     * doing it directly at the persistence layer but allows to apply business
     * logic to the calculation, for instance, as a Strategy pattern
     *
     * @return the total expenditure of the current week
     */
    public Money getCurrentWeekExpenditure() {
        ExpenseRepository repo = Persistence.buildPersistence()
                .expenseRepository();
        int year = DateTime.currentYear();
        int week = DateTime.currentWeekNumber();
        Date start = DateTime.beginningOfWeek(year, week).getTime();
        Date end = DateTime.endOfWeek(year, week).getTime();

        ExpenseRecord expenseRecord = new ExpenseRecord(
                repo.between(start, end));
        return expenseRecord.getExpenditure();
    }

    /**
     * gets the expenditure of the current month.
     *
     * this methods relies on the repository to do the calculation. for
     * performance reasons this type of aggregated calculations can be done
     * directly by the database/persistence layer
     *
     * @return the total expenditure of the current month
     */
    public Money getCurrentMonthExpenditure() {
        ExpenseRepository repo = Persistence.buildPersistence()
                .expenseRepository();
        return repo.expenditureOfMonth(DateTime.currentYear(),
                DateTime.currentMonth());
    }

    /**
     * gets the current balance.
     *
     * @return
     */
    public Money getBalance() {
        final CheckingAccountRepository repo = Persistence
                .buildPersistence().checkingAccountRepository();
        final CheckingAccount account = repo.theAccount();

        return account.balance();
    }

//    public Money getCurrentMonthExpenditure() {
//        final MonthPage month = getCurrentMonthPage();
//        return month.expenditure();
//    }
//    public Money getBalance() {
//        final BookRepository repo = Persistence.getRepositoryFactory().
//                getBookRepository();
//
//        final Book theBook = repo.theBook();
//        return theBook.currentMonth().balance();
//    }
//    private MonthPage getCurrentMonthPage() {
//        final BookRepository repo = Persistence.getRepositoryFactory().
//                getBookRepository();
//
//        final Book theBook = repo.theBook();
//        final MonthPage month = theBook.currentMonth();
//        return month;
//    }
}
