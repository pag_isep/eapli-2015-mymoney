/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.domain.limits.AlertLimit;
import eapli.mymoney.domain.limits.AlertLimitByExpenseType;
import eapli.mymoney.domain.limits.AlertLimitExpenditure;
import eapli.mymoney.domain.limits.AlertLimitType;
import eapli.mymoney.persistence.AlertLimitRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.List;

/**
 *
 * @author mcn
 */
public class ConfigureAlertLimitsController extends BaseController {

    public AlertLimitType[] getAlertLimitTypes() {
        return AlertLimitType.values();
    }

    public List<ExpenseType> getAllExpenseTypes() {
        return Persistence.buildPersistence().
                expenseTypeRepository().all();
    }

    public AlertLimit findByAlertType(AlertLimitType aLertType) {
        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        return repo.findByAlertType(aLertType);
    }

    public AlertLimit findByExpenseType(ExpenseType eT) {
        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        return repo.findByExpenseType(eT);
    }

    public void registerAlertLimitExpenditure(AlertLimitType alertType,
                                              Money yellowLimit,
                                              Money redLimit) {
        AlertLimitExpenditure limit = new AlertLimitExpenditure(alertType, yellowLimit, redLimit);
        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        repo.save(limit);
    }

    public void registerAlertLimitByExpenseType(AlertLimitType alertType,
                                                double yellowLimitPercentage,
                                                double redLimitPercentage,
                                                ExpenseType eT) {
        AlertLimitByExpenseType limit = new AlertLimitByExpenseType(alertType, yellowLimitPercentage, redLimitPercentage, eT);
        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        repo.save(limit);
    }

    public void updateAlertLimitExpenditure(
            AlertLimitExpenditure alertLimitExpenditure,
            Money yellow,
            Money red) {
        alertLimitExpenditure.updateLimits(yellow, red);

        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        repo.save(alertLimitExpenditure);

    }

    public void updateAlertLimitByExpenseType(
            AlertLimitByExpenseType alertLimitByExpenseType,
            double yellow,
            double red) {
        alertLimitByExpenseType.updateLimits(yellow, red);
        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        repo.save(alertLimitByExpenseType);
    }
}
