/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.movements.IncomeType;
import eapli.mymoney.persistence.IncomeTypeRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ListIncomeTypesController extends BaseController {

    public List<IncomeType> getIncomeTypes() {
        IncomeTypeRepository repo = Persistence.buildPersistence().incomeTypeRepository();
        return repo.all();
    }
}
