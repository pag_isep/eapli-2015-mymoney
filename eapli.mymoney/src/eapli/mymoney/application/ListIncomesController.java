/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.domain.movements.Income;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ListIncomesController extends BaseController {

    /*
     * public List<Income> getIncomes() {
     * IncomeRepository repo =
     * PersistenceRegistry.instance().incomeRepository();
     * return repo.all();
     * }
     */
    public List<Income> getIncomes() {
        CheckingAccountRepository repo = Persistence.buildPersistence().checkingAccountRepository();
        CheckingAccount account = repo.theAccount();
        return account.incomes();
    }
}
