/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.paymentmeans.PaymentMean;
import eapli.mymoney.persistence.Persistence;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ListPaymentMeansController extends BaseController {

    public List<PaymentMean> getPaymentMeans() {
        return Persistence.buildPersistence().paymentMeanRepository().all();
    }
}
