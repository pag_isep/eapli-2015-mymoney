/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.savings.SavingGoal;
import eapli.mymoney.domain.savings.SavingPlan;
import eapli.mymoney.persistence.Persistence;
import eapli.mymoney.persistence.SavingPlanRepository;
import java.util.List;

/**
 *
 * @author AJS
 */
public class ListSavingGoalsController extends BaseController {

    public List<SavingGoal> getSavingGoals() {
        SavingPlanRepository repo = Persistence.buildPersistence().savingPlanRepository();
        SavingPlan savingPlan = repo.theSavingPlan();
        return savingPlan.getSavingGoals();
    }
}
