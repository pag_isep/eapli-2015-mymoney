/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.limits.BalanceLimit;
import eapli.mymoney.persistence.LimitRepository;
import eapli.mymoney.persistence.Persistence;
//import eapli.mymoney.domain.Money;

/**
 * Controls the use case for introducing limits over balance.
 *
 * @author nuno
 */
public class RegisterBalanceLimitController extends BaseController {

    /**
     * Connection to Limits repository.
     */
    private transient LimitRepository limitRepository = null;

    /**
     * Temporary yellowLimit.
     */
    private Money yellowLimit;

    /**
     * Temporary redLimit.
     */
    private Money redLimit;

    /**
     * Override existing Yellow Limit value.
     *
     * @param yellowLimit Yellow limit value
     */
    public void setYellowLimit(Money yellowLimit) {
        //TODO controllers should be stateful
        this.yellowLimit = yellowLimit;
    }

    /**
     * Override existing Red Limit value.
     *
     * @param redLimit Red limit value
     */
    public void setRedLimit(Money redLimit) {
        //TODO controllers should be stateful
        this.redLimit = redLimit;
    }

    /**
     * Submit the new Balance Limit to the system.
     */
    public void submit() {
		//FIXME this method should have a meanngful busines name,
        // e.g., registerLimitOnBalance(yellow, red)
        final BalanceLimit newBalanceLimit = new BalanceLimit(yellowLimit, redLimit);

        final LimitRepository limitRepo = getLimitRepository();

        // FIXME we should check if we should add new balance limits or if there should be only one
        limitRepo.setBalanceLimit(newBalanceLimit);
    }

    /**
     * Obtain a Limits repository.
     *
     * @return some Limit Repository
     */
    protected LimitRepository getLimitRepository() {
        if (limitRepository == null) {
            limitRepository = Persistence.getRepositoryFactory().
                    getLimitRepository();
        }
        return limitRepository;
    }
}
