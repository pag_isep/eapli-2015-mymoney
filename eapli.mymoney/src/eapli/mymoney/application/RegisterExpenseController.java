/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.domain.paymentmeans.Cheque;
import eapli.mymoney.domain.movements.ChequePayment;
import eapli.mymoney.domain.movements.Expense;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.domain.movements.Payment;
import eapli.mymoney.domain.paymentmeans.PaymentMean;
import eapli.mymoney.domain.exceptions.InsufficientBalanceException;
import eapli.mymoney.domain.watchdog.LimitsWatchDog;
import eapli.mymoney.domain.watchdog.WatchDog;
import eapli.mymoney.domain.watchdog.WatchDogFactory;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Calendar;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class RegisterExpenseController extends BaseController {

    private Observer alertObserver = null;

    /**
     * Add a UI as an object that wants to be an observer.
     *
     * @param ui UI observer
     */
    public void addObserver(Observer alertObserver) {
        this.alertObserver = alertObserver;
    }

    /**
     * Checks if there is an UI that want to be an observer.
     *
     * @return True if the UI wishes to be an observer, False otherwise.
     */
    private boolean hasUIObserver() {
        return alertObserver != null;
    }

    /**
     * Delegates the observer if the UI wishes to receive notifications
     */
    private void delegateObserverTo(Observable observable) {
        if (hasUIObserver()) {
            WatchDog watchDog = WatchDogFactory.instance().buildWatchDogLimits(alertObserver);
            observable.addObserver(watchDog);
        }
    }

    private LimitsWatchDog watchDog;

    public RegisterExpenseController() {
    }

    public Payment createPayment(PaymentMean mean) {
        Payment payment = new Payment(mean);
        return payment;
    }

    public ChequePayment createChequePayment(Cheque mean, String checkNumber) {
        ChequePayment payment = new ChequePayment(mean, checkNumber);
        return payment;
    }

    public void registerExpense(String what, Calendar date, Money amount,
                                ExpenseType expenseType,
                                Payment payment)
            throws InsufficientBalanceException {
        Expense expense = new Expense(what, date, amount, expenseType, payment);
        // ExpenseRepository repo =
        // PersistenceRegistry.instance().expenseRepository();
        CheckingAccountRepository repo = Persistence
                .buildPersistence().checkingAccountRepository();
        CheckingAccount account = repo.theAccount();
        // OBSERVER PATTERN: Register LimitsWatchDog as account observer
        account.addObserver(watchDog);
        account.registerExpense(expense);
        repo.save(account);
    }

    /*
     * public void registerExpense(final String what,
     * final Calendar when,
     * final Money howMuch,
     * final ExpenseType type,
     * final PaymentMean paymentMean,
     * final String chequeNumber) {
     * final BookRepository bookRepo = Persistence.getRepositoryFactory().
     * getBookRepository();
     *
     * final Book theBook = bookRepo.theBook();
     *
     * delegateObserverTo(theBook);
     *
     * Recalculator theRecalculator = new Recalculator();
     * theBook.addObserver(theRecalculator);
     *
     * MonthPage month = theBook.
     * registerExpense(what, howMuch, when, type, paymentMean, chequeNumber);
     *
     * final MonthPageRepository pagesRepo = Persistence.getRepositoryFactory().
     * getMonthPageRepository();
     *
     * pagesRepo.save(month);
     * }
     */
    public List<ExpenseType> getExpenseTypes() {
        // use the existing controller to avoid duplication
        ListExpenseTypesController listExpenseTypesController = new ListExpenseTypesController();
        return listExpenseTypesController.getAllExpenseTypes();
    }

    public List<PaymentMean> getPaymentMeans() {
        return Persistence.getRepositoryFactory().getWalletRepository().
                theWallet().paymentMeans();
    }

//    public List<PaymentMean> getPaymentMeans() {
//        ListPaymentMeansController listPaymentMeansController = new ListPaymentMeansController();
//        return listPaymentMeansController.getPaymentMeans();
//    }
    // OBSERVER pattern : Delegate in ObserverFactory to register UI as observer
    public void addObserverRegisterExpense(Observer ui) {
        watchDog = WatchDogFactory.instance().buildWatchDogLimits(ui);
    }

}
