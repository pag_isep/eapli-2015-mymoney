/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.domain.movements.Income;
import eapli.mymoney.domain.movements.IncomeType;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class RegisterIncomeController extends BaseController {

    public void registerIncome(String what, Calendar date, Money amount,
                               IncomeType type) {
        Income income = new Income(what, date, amount, type);
        //IncomeRepository repo = PersistenceRegistry.instance().incomeRepository();
        CheckingAccountRepository repo = Persistence.
                buildPersistence().checkingAccountRepository();
        CheckingAccount account = repo.theAccount();
        account.registerIncome(income);
        repo.save(account);
    }

    public List<IncomeType> getIncomeTypes() {
        // use existing controller to avoid duplication
        ListIncomeTypesController listIncomeTypesController = new ListIncomeTypesController();
        return listIncomeTypesController.getIncomeTypes();
    }
}
