/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.movements.IncomeType;
import eapli.mymoney.persistence.IncomeTypeRepository;
import eapli.mymoney.persistence.Persistence;

/**
 *
 * @author Nuno
 */
public class RegisterIncomeTypeController extends BaseController {

    public void registerIncomeType(final String code,
                                   final String incomeTypeText) {
        final IncomeType incomeType = new IncomeType(code, incomeTypeText);
        final IncomeTypeRepository repo = Persistence.getRepositoryFactory().
                incomeTypeRepository();
        repo.save(incomeType);
    }
}
