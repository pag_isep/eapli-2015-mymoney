/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.domain.movements.InitialBalance;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Calendar;

/**
 *
 * @author arocha
 */
public class RegisterInitialBalanceController extends BaseController {

//    public void registerInitialBalance(final Calendar startDate,
//                                       final Money howMuch) {
//        final BookRepository repo = Persistence.getRepositoryFactory().
//                getBookRepository();
//
//        final Book theBook = repo.theBook();
//        theBook.registerInitialBalance(startDate, howMuch);
//        repo.save(theBook);
//    }
//    public boolean existsInitialBalance() {
//        final BookRepository repo = Persistence.getRepositoryFactory().
//                getBookRepository();
//
//        final Book theBook = repo.theBook();
//        return theBook.hasInitialBalance();
//    }
    public boolean existsInitialBalance() {
        CheckingAccountRepository repo = Persistence
                .buildPersistence().checkingAccountRepository();
        CheckingAccount account = repo.theAccount();
        return account.hasInitialBalance();
    }

    public void registerInitialBalance(Calendar date, Money amount) {
        InitialBalance initial = new InitialBalance(date, amount);
        CheckingAccountRepository repo = Persistence
                .buildPersistence().checkingAccountRepository();
        CheckingAccount account = repo.theAccount();
        account.registerInitialBalance(initial);
        repo.save(account);
    }
}
