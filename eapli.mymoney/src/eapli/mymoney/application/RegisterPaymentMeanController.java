/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.mymoney.domain.paymentmeans.Wallet;
import eapli.mymoney.persistence.Persistence;
import eapli.mymoney.persistence.WalletRepository;
import java.util.Calendar;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public class RegisterPaymentMeanController extends BaseController {

    public void registerBankCheckBook(final String chequebookName,
                                      final String bankName,
                                      final String accountNumber) {
        final WalletRepository repo = Persistence.getRepositoryFactory().
                getWalletRepository();

        Wallet theWallet = repo.theWallet();
        theWallet.registerBankCheckBook(chequebookName, bankName, accountNumber);

        repo.save(theWallet);
    }

    public void registerCreditCard(String bankName, Calendar validity,
                                   String accountNumber,
                                   String issuerName) {
        final WalletRepository repo = Persistence.getRepositoryFactory().
                getWalletRepository();

        final Wallet theWallet = repo.theWallet();
        theWallet.
                registerCreditCard(bankName, validity, accountNumber, issuerName);

        repo.save(theWallet);
    }

    public void registerDebitCard(String bankName, Calendar validity,
                                  String accountNumber) {
        final WalletRepository repo = Persistence.getRepositoryFactory().
                getWalletRepository();

        final Wallet theWallet = repo.theWallet();
        theWallet.registerDebitCard(bankName, validity, accountNumber);

        repo.save(theWallet);
    }

//	public void ensureCashExists() {
//		final WalletRepository repo = Persistence.getRepositoryFactory().
//			getWalletRepository();
//
//		final Wallet theWallet = repo.theWallet();
//		theWallet.ensureCashExists();
//
//		repo.save(theWallet);
//	}
}
