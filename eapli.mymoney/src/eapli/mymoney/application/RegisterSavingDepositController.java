/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.domain.savings.SavingDeposit;
import eapli.mymoney.domain.savings.SavingGoal;
import eapli.mymoney.domain.savings.SavingPlan;
import eapli.mymoney.domain.exceptions.InsufficientBalanceException;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.Persistence;
import eapli.mymoney.persistence.SavingPlanRepository;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author ajs
 */
public class RegisterSavingDepositController extends BaseController {

    public RegisterSavingDepositController() {
    }

    public void registerSavingDeposit(SavingGoal goal, Calendar date,
                                      Money amount, String description) throws InsufficientBalanceException {
        CheckingAccountRepository checkingAccountRepository = Persistence.
                buildPersistence().checkingAccountRepository();
        CheckingAccount checkingAccount = checkingAccountRepository.theAccount();

        try {
            SavingDeposit savingDeposit = new SavingDeposit(description, date, amount);

            SavingPlanRepository savingPlanRepository = Persistence.
                    buildPersistence().savingPlanRepository();
            SavingPlan savingPlan = savingPlanRepository.theSavingPlan();

            // TODO should we call two different register methods or shuld the
            // operation be encapsulated inside the "main" object?
            // where do we draw the line in our aggregates?
            checkingAccount.registerSavingDeposit(savingDeposit);
            savingPlan.registerSavingDeposit(savingDeposit, goal);

            savingPlanRepository.save(savingPlan);
            checkingAccountRepository.save(checkingAccount);
        }
        catch (InsufficientBalanceException ex) {
            throw ex;
        }
    }

    // TODO avoid duplication with RegisterSavingWithdrawController
    public List<SavingGoal> getSavingGoals() {
        // TODO should a controller create other controller objects?
        // to avoid duplication we migth encapsulate this method in another
        // class which is not a controller
        return new ListSavingGoalsController().getSavingGoals();
    }
}
