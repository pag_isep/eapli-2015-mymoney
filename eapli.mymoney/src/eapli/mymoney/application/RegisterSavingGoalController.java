/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.savings.SavingGoal;
import eapli.mymoney.domain.savings.SavingPlan;
import eapli.mymoney.persistence.Persistence;
import eapli.mymoney.persistence.SavingPlanRepository;

/**
 *
 * @author losa
 */
public class RegisterSavingGoalController extends BaseController {

    public void registerSavingGoal(String targetDescription,
                                   Money targetAmount) {
        SavingPlanRepository repo = Persistence.buildPersistence().savingPlanRepository();

        SavingGoal savingGoal = new SavingGoal(targetDescription, targetAmount);

        SavingPlan savingsplan = repo.theSavingPlan();
        savingsplan.registerSavingGoal(savingGoal);
        repo.save(savingsplan);
    }
}
