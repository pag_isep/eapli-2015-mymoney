/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.application;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.domain.savings.SavingGoal;
import eapli.mymoney.domain.savings.SavingPlan;
import eapli.mymoney.domain.savings.SavingWithdraw;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.Persistence;
import eapli.mymoney.persistence.SavingPlanRepository;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author AJS
 */
public class RegisterSavingWithdrawController extends BaseController {

    public RegisterSavingWithdrawController() {
    }

    public void registerSavingWithdraw(SavingGoal goal, Calendar date,
                                       Money amount, String description) {
        SavingPlanRepository savingPlanRepository = Persistence
                .buildPersistence().savingPlanRepository();
        SavingPlan savingPlan = savingPlanRepository.theSavingPlan();

        // FIX controllers shouldn't have business logic
        if (goal.enoughSavings(amount)) {

            CheckingAccountRepository checkingAccountRepository = Persistence.
                    buildPersistence().checkingAccountRepository();
            CheckingAccount checkingAccount = checkingAccountRepository
                    .theAccount();

            SavingWithdraw savingWithdraw = new SavingWithdraw(description,
                    date, amount);

            // TODO should we call two different register methods or should the
            // operation be encapsulated inside the "main" object?
            // where do we draw the line in our aggregates?
            savingPlan.registerSavingWithdraw(savingWithdraw, goal);
            checkingAccount.registerSavingWithdraw(savingWithdraw);

            savingPlanRepository.save(savingPlan);
            checkingAccountRepository.save(checkingAccount);
        }
    }

    // TODO avoid duplication with RegisterSavingDepositController
    public List<SavingGoal> getSavingGoals() {
        // TODO should a controller create other controller objects?
        // to avoid duplication we migth encapsulate this method in another
        // class which is not a controller
        return new ListSavingGoalsController().getSavingGoals();
    }
}
