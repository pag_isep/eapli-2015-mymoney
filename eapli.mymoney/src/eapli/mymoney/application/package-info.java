/**
 * Provides the use case controllers of the application.
 *
 * @author Paulo Gandra Sousa
 *
 */
package eapli.mymoney.application;
