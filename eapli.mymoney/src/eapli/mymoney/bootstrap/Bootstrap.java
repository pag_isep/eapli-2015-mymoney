/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.bootstrap;

/**
 *
 * @author Paulo Gandra Sousa
 */
public interface Bootstrap {

    /**
     * ensure the desired bootstrap data exists or if not create it
     */
    void bootstrap();
}
