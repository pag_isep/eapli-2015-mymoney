/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.bootstrap;

import eapli.framework.domain.Money;
import eapli.mymoney.application.RegisterBalanceLimitController;

/**
 * Limit Bootstrapper
 *
 * @author Nuno Bettencourt <nmb@isep.ipp.pt>
 */
public class LimitBootstrapper {

	/**
	 * Sets some dummy limits
	 */
	public void execute() {
		final RegisterBalanceLimitController controller = new RegisterBalanceLimitController();

		controller.setRedLimit(Money.euros(15));
		controller.setYellowLimit(Money.euros(20));
		controller.submit();
	}

}
