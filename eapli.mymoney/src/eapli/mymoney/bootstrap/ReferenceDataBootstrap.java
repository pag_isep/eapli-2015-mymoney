/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.bootstrap;

import eapli.mymoney.domain.paymentmeans.Cash;
import static eapli.mymoney.domain.paymentmeans.Cash.EUR;
import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.domain.movements.IncomeType;
import eapli.mymoney.domain.savings.SavingPlan;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.IncomeTypeRepository;
import eapli.mymoney.persistence.PaymentMeanRepository;
import eapli.mymoney.persistence.Persistence;
import eapli.mymoney.persistence.SavingPlanRepository;
import java.util.Date;

/**
 * this classes serves as bootstrap data loader. just to make sure some data
 * exists in order to use the system it should be removed for "production-ready"
 * deployment
 *
 * @author Paulo Gandra Sousa
 */
public class ReferenceDataBootstrap implements Bootstrap {

    public static final String SALARY_INCOME_TYPE = "Sal.";
    private static final String SALARY_INCOME_TYPE_DESC = "Salary";
    public final static String CLOTHING_EXPENSE_TYPE = "Cloth.";
    private final static String CLOTHING_EXPENSE_TYPE_DESC = "Clothing";
    public final static String TRANSPORTS_EXPENSE_TYPE = "Trans.";
    private final static String TRANSPORTS_EXPENSE_TYPE_DESC = "Transports";

    private void ensureClothingExpenseTypeExists(ExpenseTypeRepository repo) {
        ExpenseType clothing = repo.findById(CLOTHING_EXPENSE_TYPE);
        if (clothing == null) {
            clothing = new ExpenseType(CLOTHING_EXPENSE_TYPE, CLOTHING_EXPENSE_TYPE_DESC);
            repo.save(clothing);
        }
    }

    private void ensureTransportsExpenseTypeExists(ExpenseTypeRepository repo) {
        ExpenseType transports = repo.findById(TRANSPORTS_EXPENSE_TYPE);
        if (transports == null) {
            transports = new ExpenseType(TRANSPORTS_EXPENSE_TYPE, TRANSPORTS_EXPENSE_TYPE_DESC);
            repo.save(transports);
        }
    }

    private void ensureCashEurExists() {
        try {
            PaymentMeanRepository repo = Persistence.
                    buildPersistence().paymentMeanRepository();
            Cash eur = repo.getCash(EUR);
        }
        catch (IllegalStateException ex) {
            // if there is no Cash, then we must stop
            throw ex;
        }
    }

    private void ensureTheAccountExists() {
        CheckingAccountRepository repo = Persistence.
                buildPersistence().checkingAccountRepository();
        try {
            repo.theAccount();
        }
        catch (IllegalStateException ex) {
            CheckingAccount theAccount = new CheckingAccount();
            repo.save(theAccount);
        }
    }

    private void ensureSavingsPlanExists() {
        SavingPlanRepository repo = Persistence.buildPersistence().
                savingPlanRepository();
        try {
            repo.theSavingPlan();
        }
        catch (IllegalStateException ex) {
            SavingPlan theSavingPlan = new SavingPlan(new Date());
            repo.save(theSavingPlan);
        }
    }

    private void ensureDefaultExpenseTypesExist() {
        ExpenseTypeRepository repo = Persistence.
                buildPersistence().expenseTypeRepository();
        ensureClothingExpenseTypeExists(repo);
        ensureTransportsExpenseTypeExists(repo);
    }

    private void ensureDefaultIncomeTypesExist() {
        IncomeTypeRepository repo = Persistence.buildPersistence().
                incomeTypeRepository();
        ensureSalaryIncomeTypeExists(repo);
    }

    private void ensureSalaryIncomeTypeExists(IncomeTypeRepository repo) {
        IncomeType salary = repo.findById(SALARY_INCOME_TYPE);
        if (salary == null) {
            salary = new IncomeType(SALARY_INCOME_TYPE, SALARY_INCOME_TYPE_DESC);
            repo.save(salary);
        }
    }

    @Override
    public void bootstrap() {
        ensureTheAccountExists();
        ensureDefaultExpenseTypesExist();
        ensureDefaultIncomeTypesExist();
        ensureCashEurExists();
        ensureSavingsPlanExists();
    }
}
