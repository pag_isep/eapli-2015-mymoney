/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.bootstrap;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.domain.limits.AlertLimitByExpenseType;
import eapli.mymoney.domain.limits.AlertLimitExpenditure;
import static eapli.mymoney.domain.limits.AlertLimitType.LIMIT_DEVIATION_BY_EXPENSE_TYPE;
import static eapli.mymoney.domain.limits.AlertLimitType.LIMIT_MINIMUM_BALANCE;
import static eapli.mymoney.domain.limits.AlertLimitType.LIMIT_MONTH_EXPENDITURE;
import static eapli.mymoney.domain.limits.AlertLimitType.LIMIT_WEEK_EXPENDITURE;
import eapli.mymoney.persistence.AlertLimitRepository;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.List;

/**
 *
 * @author mcn
 */
public class SomeDefaultAlertLimitBootstrap implements Bootstrap {

    private static final Money LIMIT_WEEK_YELLOW = Money.euros(100);
    private static final Money LIMIT_WEEK_RED = Money.euros(500);
    private static final Money LIMIT_MONTH_YELLOW = Money.euros(600);
    private static final Money LIMIT_MONTH_RED = Money.euros(2000);
    private static final double LIMIT_DEVIATION_YELLOW = 0.1;
    private static final double LIMIT_DEVIATION_RED = 0.5;
    private static final Money LIMIT_BALANCE_YELLOW = Money.euros(1000);
    private static final Money LIMIT_BALANCE_RED = Money.euros(500);

    @Override
    public void bootstrap() {
        ensureLimitWeekExpenditureExists();

        ensureLimitMonthExpenditureExists();

        ensureLimitDeviationExists();

        ensureLimitMinimumBalanceExists();
    }

    private void ensureLimitWeekExpenditureExists() {
        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        if (repo.findByAlertType(LIMIT_WEEK_EXPENDITURE) == null) {
            AlertLimitExpenditure alertLimitWeekExpenditure = new AlertLimitExpenditure(
                    LIMIT_WEEK_EXPENDITURE, LIMIT_WEEK_YELLOW, LIMIT_WEEK_RED);
            repo.save(alertLimitWeekExpenditure);
        }
    }

    private void ensureLimitMonthExpenditureExists() {
        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        if (repo.findByAlertType(LIMIT_MONTH_EXPENDITURE) == null) {
            AlertLimitExpenditure alertLimitMonthExpenditure = new AlertLimitExpenditure(
                    LIMIT_MONTH_EXPENDITURE, LIMIT_MONTH_YELLOW, LIMIT_MONTH_RED);
            repo.save(alertLimitMonthExpenditure);
        }
    }

    private void ensureLimitDeviationExists() {
        ExpenseTypeRepository repoET = Persistence
                .buildPersistence().expenseTypeRepository();
        List<ExpenseType> expenseTypes = repoET.all();
        for (ExpenseType eT : expenseTypes) {
            AlertLimitByExpenseType alertLimitET = new AlertLimitByExpenseType(
                    LIMIT_DEVIATION_BY_EXPENSE_TYPE, LIMIT_DEVIATION_YELLOW,
                    LIMIT_DEVIATION_RED, eT);
            AlertLimitRepository repoAL = Persistence
                    .buildPersistence().alertLimitRepository();
            repoAL.save(alertLimitET);
        }
    }

    private void ensureLimitMinimumBalanceExists() {
        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        if (repo.findByAlertType(LIMIT_MINIMUM_BALANCE) == null) {
            AlertLimitExpenditure alertLimitMonthExpenditure = new AlertLimitExpenditure(
                    LIMIT_MINIMUM_BALANCE, LIMIT_BALANCE_YELLOW, LIMIT_BALANCE_RED);
            repo.save(alertLimitMonthExpenditure);
        }
    }
}
