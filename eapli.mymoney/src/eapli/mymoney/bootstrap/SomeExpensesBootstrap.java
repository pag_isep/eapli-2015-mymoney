/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.bootstrap;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.paymentmeans.Cash;
import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.domain.movements.Expense;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.domain.movements.Payment;
import eapli.mymoney.domain.exceptions.InsufficientBalanceException;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.PaymentMeanRepository;
import eapli.mymoney.persistence.Persistence;
import eapli.util.DateTime;
import java.util.Calendar;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class SomeExpensesBootstrap implements Bootstrap {

    @Override
    public void bootstrap() {
        CheckingAccountRepository repoAccount = Persistence
                .buildPersistence().checkingAccountRepository();
        CheckingAccount theAccount = repoAccount.theAccount();

        ExpenseTypeRepository repoExpenseType = Persistence
                .buildPersistence().expenseTypeRepository();
        ExpenseType clothing = repoExpenseType
                .findById(ReferenceDataBootstrap.CLOTHING_EXPENSE_TYPE);
        ExpenseType transport = repoExpenseType
                .findById(ReferenceDataBootstrap.TRANSPORTS_EXPENSE_TYPE);

        PaymentMeanRepository repoPaymentMethod = Persistence
                .buildPersistence().paymentMeanRepository();
        Cash cashEur = repoPaymentMethod.getCash(Cash.EUR);
        Payment payment = new Payment(cashEur);

        Calendar dateOfExpense = DateTime.now();
        try {
            Expense exp = new Expense("sapatilhas", dateOfExpense,
                    Money.euros(100), clothing, payment);
            theAccount.registerExpense(exp);

            exp = new Expense("T-shirt", dateOfExpense,
                    Money.euros(10), clothing, payment);
            theAccount.registerExpense(exp);

            dateOfExpense.add(Calendar.DAY_OF_MONTH, 4);

            exp = new Expense("calcas", dateOfExpense,
                    Money.euros(150), clothing, payment);
            theAccount.registerExpense(exp);

            dateOfExpense.add(Calendar.DAY_OF_MONTH, -30);

            exp = new Expense("passe Metro", dateOfExpense,
                    Money.euros(35), transport, payment);
            theAccount.registerExpense(exp);
        }
        catch (InsufficientBalanceException ex) {
            System.out
                    .println("unable to bootstrap all or some of the expenses due to insufficient balance");
        }
        repoAccount.save(theAccount);
    }
}
