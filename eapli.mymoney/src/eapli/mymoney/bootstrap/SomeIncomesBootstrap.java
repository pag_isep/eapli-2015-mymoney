/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.bootstrap;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.domain.movements.Income;
import eapli.mymoney.domain.movements.IncomeType;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.IncomeTypeRepository;
import eapli.mymoney.persistence.Persistence;
import eapli.util.DateTime;
import java.util.Calendar;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class SomeIncomesBootstrap implements Bootstrap {

    @Override
    public void bootstrap() {
        CheckingAccountRepository repoAccount = Persistence
                .buildPersistence().checkingAccountRepository();
        CheckingAccount theAccount = repoAccount.theAccount();

        IncomeTypeRepository repoIncomeType = Persistence
                .buildPersistence().incomeTypeRepository();
        IncomeType salary = repoIncomeType
                .findById(ReferenceDataBootstrap.SALARY_INCOME_TYPE);

        Calendar dateOfIncome = DateTime.now();

        Income inc = new Income("ordenado deste mês", dateOfIncome,
                Money.euros(1000), salary);
        theAccount.registerIncome(inc);

        repoAccount.save(theAccount);
    }
}
