/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.domain.Money;
import eapli.framework.patterns.AggregateRoot;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.domain.movements.InitialBalance;
import eapli.mymoney.domain.paymentmeans.PaymentMean;
import eapli.mymoney.persistence.MonthPageRepository;
import eapli.mymoney.persistence.Persistence;
import eapli.util.DateTime;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 * Represents an expense/income book
 *
 * Note that traversal of month page is done thru the explicit use of the
 * repository class
 *
 * @author Paulo Gandra Sousa
 */
@Deprecated
@Entity
public class Book extends Observable
        implements AggregateRoot<BookID>, Serializable {

    @Id
    @GeneratedValue
    private Long surrogatekey;

    @Embedded
    private final BookID id;

    // TODO considerar tornar este objeto Embeddable
    @OneToOne(cascade = CascadeType.ALL)
    private InitialBalance theInitialBalance = null;

    protected Book() {
        // for ORM tool
        this.id = new BookID("***DUMMY***");
    }

    public Book(BookID id) {
        this.id = id;
    }

    /**
     * returns the MonthPage for the current system date
     *
     * @return the MonthPage for the current system date
     */
    public MonthPage currentMonth() {
        return findOrCreatePages(DateTime.now());
    }

    public MonthPage registerExpense(final String what, final Money howMuch,
                                     final Calendar when, final ExpenseType type,
                                     final PaymentMean paymentMean, String info) {
        final MonthPage month = findOrCreatePages(when);
        month.addObserverForExpensesRegistered(new MonthPageObserver());
        month.registerExpense(what, howMuch, when, type, paymentMean, info);
        return month;
    }

    public MonthPage findOrCreatePages(final Calendar when) {
        //  monthPage, the page of the date of the Movement, already exists or will be created
        MonthPage monthPage = searchMonthPage(when);
        if (monthPage == null) {
            monthPage = createPagesBeforeMovement(when);
        }
        return monthPage;
    }

    private MonthPage searchMonthPage(final Calendar when) {
        final MonthPageRepository repo = Persistence.getRepositoryFactory().
                getMonthPageRepository();
        MonthPage monthPage = repo.findForDate(when);
        return monthPage;
    }

    private MonthPage createMonthPage(Calendar date, Money carriedBalance) {
        MonthPage monthPage = new MonthPage(date, carriedBalance);
        return monthPage;
    }

    // create pages before movement and returns the monthPage of the movement
    private MonthPage createPagesBeforeMovement(Calendar when) {
        MonthPage latestMonthPage = findMostRecentPage();

        //Calendar's month starts at 0, so the next month is latestMonthPage.getMonth()
        Calendar nextMonth = new GregorianCalendar(latestMonthPage.getYear(),
                latestMonthPage.getMonth(),
                1);

        Money balanceToCarry = latestMonthPage.balance();

        MonthPage monthPage = createMonthPages(nextMonth, when, balanceToCarry);
        return monthPage;
    }

    private MonthPage findMostRecentPage() {
        final MonthPageRepository pagesRepo
                = Persistence.getRepositoryFactory().getMonthPageRepository();
        MonthPage latestMonthPage = pagesRepo.findMostRecentMonthPage();
        // TODO podemos considerar criar esta página inicial na altura em que
        // registamos o saldo inicial e dessa forma simplificamos este método
        // que é chamado com frequencia
        if (latestMonthPage == null) {
            latestMonthPage = createMonthPage(theInitialBalance.referenceDate(),
                    theInitialBalance.value());
            pagesRepo.save(latestMonthPage);
        }
        return latestMonthPage;
    }

    // create monthPages and returns the monthPage of the latest date
    private MonthPage createMonthPages(Calendar initialDate, Calendar finalDate,
                                       Money carriedBalance) {
        //initialDate.add(Calendar.MONTH, 1);
        //finalDate.add(Calendar.MONTH, 1);
        Calendar date = initialDate;
        MonthPage monthPage = null;
        final MonthPageRepository pagesRepo
                = Persistence.getRepositoryFactory().getMonthPageRepository();
        do {
            monthPage = createMonthPage(date, carriedBalance);
            // TODO the save method opens and closes a transaction each time it is called,
            // this might result in inefficent code when called inside a loop
            pagesRepo.save(monthPage);
            date.add(Calendar.MONTH, 1);
        } while (yearMonthBefore(date, finalDate));

        return monthPage;
    }

    private boolean yearMonthBefore(Calendar one, Calendar another) {
        return one.get(Calendar.YEAR) <= another.get(Calendar.YEAR)
                && one.get(Calendar.MONTH) <= another.get(Calendar.MONTH);
    }

    /**
     * returns the list of month pages for this book.
     *
     * @return the list of month pages for this book
     */
    public List<MonthPage> monthPages() {
        final MonthPageRepository repo = Persistence.getRepositoryFactory().
                getMonthPageRepository();
        return Collections.unmodifiableList(repo.all());
    }

    // TODO será que deviamos deixar criar o objeto sem saldo inicial
    // ou isso deveria ser um dos invariantes?
    public void registerInitialBalance(final Calendar startDate,
                                       final Money howMuch) {
        if (null == theInitialBalance) {
            theInitialBalance = new InitialBalance(startDate,
                    howMuch);
        }
        else {
            // TODO não seria melhor lançar IllegalStateException?
            // não é o argumento que é inválido, mas sim o estado atual do objeto
            // que não permite atribuir novo saldo inicial.
            // se optarmos por tornar isto um invariante e o saldo inicial
            // só puder ser definido na construção do objeto, este aspeto desaparece
            throw new IllegalArgumentException();
        }
    }

    // TODO até ao momento não há nenhum requisito que nos obrigue a expor o
    // saldo inicial para o exterior
    public InitialBalance initialBalance() {
        return theInitialBalance;
    }

    private class MonthPageObserver implements Observer {

        @Override
        public void update(Observable o, Object arg) {
            // TODO check if we receive events from different objects and need to act accordingly
            // for now we only receive ExpenseRegistered events from MonthPage
            setChanged();
            notifyObservers(arg);
        }
    }

    public boolean hasInitialBalance() {
        return (theInitialBalance != null);
    }

    @Override
    public BookID id() {
        return id;
    }
}
