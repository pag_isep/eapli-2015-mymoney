/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.patterns.ValueObject;
import javax.persistence.Embeddable;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Deprecated
@Embeddable
public class BookID implements ValueObject {

    public static BookID MY_BOOK = new BookID("MY BOOK");

    private final String name;

    protected BookID() {
        // for ORM tool;
        name = "***DUMMY***";
    }

    public BookID(String name) {
        // FIXME should validate for meaningfull values
        this.name = name;
    }
}
