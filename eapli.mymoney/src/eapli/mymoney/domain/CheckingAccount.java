/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.domain.Money;
import eapli.framework.visitor.Visitable;
import eapli.framework.visitor.Visitor;
import eapli.mymoney.domain.exceptions.InsufficientBalanceException;
import eapli.mymoney.domain.movements.Expense;
import eapli.mymoney.domain.movements.ExpenseRegistered;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.domain.movements.Income;
import eapli.mymoney.domain.movements.InitialBalance;
import eapli.mymoney.domain.movements.Movement;
import eapli.mymoney.domain.savings.SavingDeposit;
import eapli.mymoney.domain.savings.SavingWithdraw;
import eapli.mymoney.report.ExpensesReport;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Entity
public class CheckingAccount extends Observable implements Serializable,
                                                           Visitable<Movement> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    // private String owner;
    @OneToOne(cascade = CascadeType.MERGE)
    private InitialBalance initialBalance;
    // private BigDecimal balance;
    @OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(name = "CheckingAccount_Movements")
    private final List<Movement> movements;
    @OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(name = "CheckingAccount_Expenses")
    private final List<Expense> expenses;
    @OneToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(name = "CheckingAccount_Incomes")
    private final List<Income> incomes;
    // @ManyToMany
    // @ElementCollection(fetch = FetchType.EAGER)
    // @CollectionTable(name = "CheckingAccount_Expenses_by_ExpenseType")
    // @MapKeyColumn(name = "ExpenseType_ID")
    // @Column(name = "Expense_ID")
    @Transient
    private Map<ExpenseType, List<Expense>> expensesByType;

    public CheckingAccount() {
        super();
        // create empty list for using with in memory repositories
        movements = new ArrayList<>();
        expenses = new ArrayList<>();
        incomes = new ArrayList<>();
    }

    private Map<ExpenseType, List<Expense>> getExpensesByType() {
        if (expensesByType == null) {
            expensesByType = new HashMap<ExpenseType, List<Expense>>();
            reClassifyMovements();
        }
        return expensesByType;
    }

    /**
     * checks if the object already has an id assigned by the persistence layer
     * even tough this is a public method if should not be used by code other
     * than the persistence layer
     *
     * @return
     */
    public boolean hasId() {
        return id != null;
    }

    public Money totalExpenditure() {
        return sumAmount(expenses());
    }

    public Money totalEarnings() {
        return sumAmount(incomes());
    }

    public void registerIncome(Income income) {
        addMovement(income);
        classifyMovementAsIncome(income);
        // TODO should publish an event
    }

    /**
     *
     * @param theMovements
     * @return BigDecimal
     */
    private Money sumAmount(List<? extends Movement> theMovements) {
        Money sum = Money.euros(0);
        for (Movement e : theMovements) {
            sum = sum.add(e.amount());
        }
        return sum;
    }

    public void registerExpense(Expense expense)
            throws InsufficientBalanceException {
        if (!hasEnoughBalance(expense.amount())) {
            throw new InsufficientBalanceException(balance(),
                    expense.amount());
        }
        addMovement(expense);
        classifyMovementAsExpense(expense);
        classifyExpense(expense);
        publishEvent(expense);
    }

    private void publishEvent(Expense expense) {
        // ObserverPattern - Cria um evento e notifica Observers
        this.setChanged();
        ExpenseRegistered event = new ExpenseRegistered(
                expense);
        this.notifyObservers(event);
    }

    public void registerSavingDeposit(SavingDeposit savingDeposit)
            throws InsufficientBalanceException {
        if (!hasEnoughBalance(savingDeposit.amount())) {
            throw new InsufficientBalanceException(balance(),
                    savingDeposit.amount());
        }
        addMovement(savingDeposit);
        // TODO should publish an event
    }

    public void registerSavingWithdraw(SavingWithdraw savingWithdraw) {
        addMovement(savingWithdraw);
        // TODO should publish an event
    }

    private void classifyMovementAsExpense(Expense expense) {
        expenses.add(expense);
    }

    /**
     *
     * @param expense
     */
    private void classifyExpense(Expense expense) {
        List<Expense> theExpenses = getExpensesByType().get(
                expense.getExpenseType());
        if (theExpenses == null) {
            theExpenses = new ArrayList<Expense>();
            getExpensesByType().put(expense.getExpenseType(), theExpenses);
        }
        theExpenses.add(expense);
    }

    private void classifyMovementAsIncome(Income income) {
        incomes.add(income);
    }

    public List<Movement> movements() {
        return Collections.unmodifiableList(movements);
    }

    public List<Expense> expenses() {
        return Collections.unmodifiableList(expenses);
    }

    public List<Income> incomes() {
        return Collections.unmodifiableList(incomes);
    }

    /**
     * Permite reclassificar os tipos de movimentos em despesa e receitas
     */
    private void reClassifyMovements() {
        // TODO verificar se é necessário classificar os movimentos

        for (Movement movement : movements()) {
            if (movement instanceof Expense) {
                classifyExpense((Expense) movement);
            }
        }
    }

    //TODO move this method out of the domain and into the report bounded context?
    public ExpensesReport getExpensesAggregatedByType(Date inicialPeriod,
                                                      Date finalPeriod) {

        ExpensesReport expenseReport = new ExpensesReport();

        for (Expense expense : expenses()) {
            expenseReport.aggregate(expense);
        }

        return expenseReport;
    }

    //TODO move this method out of the domain and into the report bounded context?
    public Map<ExpenseType, List<Expense>> getExpensesClassifiedByExpenseType() {
        return Collections.unmodifiableMap(getExpensesByType());
    }

    private void addMovement(Movement movement) throws IllegalArgumentException {
        if (movement == null) {
            throw new IllegalArgumentException();
        }

        movements.add(movement);
    }

    private boolean hasEnoughBalance(Money amount) {
        // TODO given the name of the method is would be more logic to make the
        // comparison from the getBalance object to the amount object
        if (amount.compareTo(balance()) == 1) {
            return false;
        }

        return true;
    }

    /**
     * get the current balance of the account
     *
     * @return
     */
    public Money balance() {
        Money i = Money.euros(0);
        if (initialBalance != null) {
            i = initialBalance.value();
        }

        /*
         * calculates the balance of the account. alternatively, the balance
         * could be a persistent attribute allways up to-date
         */
        return totalEarnings().subtract(totalExpenditure()).add(i);
    }

    public void registerInitialBalance(InitialBalance initial) {
        if (initial == null) {
            throw new IllegalArgumentException();
        }
        if (hasInitialBalance()) {
            throw new IllegalStateException();
        }
        initialBalance = initial;
    }

    public boolean hasInitialBalance() {
        return initialBalance != null;
    }

    public Money averageExpenditure(ExpenseType expenseType) {
        List<Expense> expensesOfType = getExpensesByType().get(expenseType);
        if (expensesOfType == null || expensesOfType.isEmpty()) {
            return Money.euros(0);
        }
        //return sumAmount(expensesOfType).divide(new BigDecimal(expensesOfType.size()), 2, RoundingMode.UP);
        return sumAmount(expensesOfType).divide(expensesOfType.size())[0];
    }

    public Money expenditureOfMonth(int year, int month) {
        Money total = Money.euros(0);
        for (Expense expense : expenses) {
            if (expense.ocurredInMonth(year, month)) {
                total = total.add(expense.amount());
            }
        }
        return total;
    }

    public Money expenditureOfWeek(int year, int week) {
        Money total = Money.euros(0);
        for (Expense expense : expenses) {
            if (expense.ocurredInWeek(year, week)) {
                total = total.add(expense.amount());
            }
        }
        return total;
    }

    @Override
    public void accept(Visitor<Movement> visitor) {
        for (Movement aMovement : movements()) {
            visitor.beforeVisiting(aMovement);
            visitor.visit(aMovement);
            visitor.afterVisiting(aMovement);
        }
    }
}
