/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.domain.Money;
import eapli.framework.patterns.AggregateRoot;
import eapli.mymoney.domain.movements.Expense;
import eapli.mymoney.domain.movements.ExpenseRegistered;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.domain.movements.Payment;
import eapli.mymoney.domain.paymentmeans.PaymentMean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Deprecated
@Entity
public class MonthPage implements AggregateRoot<MonthPageID>, Serializable {

    // removed the (strategy = GenerationType.IDENTITY) from the key as it was
    // producing duplicate records in persist()
    @Id
    @GeneratedValue
    private Long surrogatekey;

    @Embedded
    private MonthPageID id;

    @AttributeOverrides({
        @AttributeOverride(name = "amount", column = @Column(name = "carriedBalance_amount")),
        @AttributeOverride(name = "currency", column = @Column(name = "carriedBalance_curr"))
    })
    @Embedded
    private Money carriedBalance = Money.euros(0);

    @OneToMany(cascade = CascadeType.ALL)
    private final List<Expense> expenses = new ArrayList<>();

    protected MonthPage() {
        // provided for ORM tool only.
    }

    /**
     *
     * @param year
     * @param month the month of the year (1-12)
     */
    public MonthPage(final int year, final int month, final Money carriedBalance) {
        // TODO should we use the constants defined in Calendar for month?
        // in that case, the month would be 0-11 (-1 of the "real" month)
        if (carriedBalance == null) {
            throw new IllegalArgumentException();
        }
        this.id = new MonthPageID(year, month);
        this.carriedBalance = carriedBalance;
    }

    public MonthPage(final Calendar when, final Money carriedBalance) {
        this(when.get(Calendar.YEAR), when.get(Calendar.MONTH) + 1, carriedBalance);
    }

    public void registerExpense(final String what,
                                final Money howMuch,
                                final Calendar when,
                                final ExpenseType type,
                                final PaymentMean paymentMean,
                                final String info) {
        if (!holds(when)) {
            throw new IllegalArgumentException();
        }
        String[] tags = new String[1];
        tags[0] = info;
        Expense theExpense = new Expense(what, when, howMuch, type, new Payment(paymentMean), tags);
        expenses.add(theExpense);

        publish(theExpense);
    }

    public List<Expense> expenses() {
        return Collections.unmodifiableList(expenses);
    }

    /**
     * checks if a certain date belongs to this month page
     *
     * @param when
     * @return
     */
    public boolean holds(final Calendar when) {
        return id.isOf(when.get(Calendar.YEAR), when.get(Calendar.MONTH) + 1);
    }

    public Money expenditure() {
        Money total = Money.euros(0.0);
        for (Expense exp : expenses) {
            total = total.add(exp.amount());
        }
        return total;
    }

    public Money balance() {
        return expenditure().add(carriedBalance);
    }

    /**
     * publishes an expense registered event
     *
     * @param theExpense
     */
    private void publish(Expense theExpense) {
        ExpenseRegistered event = new ExpenseRegistered(theExpense);
        expenseRegisteredPublisher.notify(event);
    }

    /**
     * a helper class to segregate the observers according to the kind of event
     */
    private class ExpenseRegisteredObservable extends Observable {

        // TODO consider moving to a package class instead of an inner class
        public void notify(ExpenseRegistered event) {
            setChanged();
            notifyObservers(event);
        }
    }

    /**
     * the helper object to implement the publishing pipeline for
     * ExpenseRegistered events
     */
    private transient final ExpenseRegisteredObservable expenseRegisteredPublisher = new ExpenseRegisteredObservable();

    /**
     * register interest in expense registered domain events
     *
     * @param interestedParty
     */
    public void addObserverForExpensesRegistered(Observer interestedParty) {
        expenseRegisteredPublisher.addObserver(interestedParty);
    }

    public boolean isOf(int year, int month) {
        return id.isOf(year, month);
    }

    @Override
    public MonthPageID id() {
        return id;
    }

    // FIXME this method should not exist: avoid getters, use methods like isOf() instead
    public int getYear() {
        return id.getYear();
    }

    // FIXME this method should not exist: avoid getters, use methods like isOf() instead
    public int getMonth() {
        return id.getMonth();
    }

    public void performBalanceCorrection(Money difInitialBalance) {
        carriedBalance = carriedBalance.add(difInitialBalance);
    }
}
