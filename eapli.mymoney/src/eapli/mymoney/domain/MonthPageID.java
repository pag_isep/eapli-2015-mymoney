/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain;

import eapli.framework.patterns.ValueObject;
import javax.persistence.Embeddable;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Deprecated
@Embeddable
public class MonthPageID implements ValueObject {

    private final int theYear; // YEAR isOf a reserved SQL word
    private final int theMonth; // MONTH isOf a reserved SQL word

    protected MonthPageID() {
        // for ORM tool
        theYear = theMonth = 0;
    }

    /**
     *
     * @param year
     * @param month the month of the year (1-12)
     */
    public MonthPageID(final int year, final int month) {
		// TODO should we use the constants defined in Calendar for month?
        // in that case, the month would be 0-11 (-1 of the "real" month)
        if (month < 1 || month > 12) {
            throw new IllegalArgumentException();
        }
        // TODO what validation to apply for year? it might make sense to limit to years after the publishing date of the app
        if (year < 0) {
            throw new IllegalArgumentException();
        }
        this.theYear = year;
        this.theMonth = month;
    }

    boolean isOf(int year, int month) {
        return (theMonth == month && theYear == year);
    }

    public int getYear() {
        return theYear;
    }

    public int getMonth() {
        return theMonth;
    }
}
