/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.balancerecalculator;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.MonthPage;
import eapli.mymoney.domain.movements.ExpenseRegistered;
import eapli.mymoney.persistence.MonthPageRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author arocha
 */
public class Recalculator implements Observer {

	@Override
	public void update(Observable o, Object o1) {
		ExpenseRegistered theExpenseRegistered;
		theExpenseRegistered = (ExpenseRegistered) o1;
		Calendar processDate = theExpenseRegistered.occurredAt();

		final MonthPageRepository pagesRepo = Persistence.
			getRepositoryFactory().getMonthPageRepository();

		MonthPage theMostRecentMonth = pagesRepo.findMostRecentMonthPage();

		if ((theMostRecentMonth.getYear() != processDate.get(Calendar.YEAR))
			|| (theMostRecentMonth.getMonth() != processDate.get(Calendar.MONTH))) {

			recalculateBalance(theExpenseRegistered.occurredAt(), theExpenseRegistered.
				theExpense().amount().multiply(-1));

		}
	}

	private void recalculateBalance(Calendar processDate,
									Money balanceCorrection) {

		final MonthPageRepository pagesRepo = Persistence.
			getRepositoryFactory().getMonthPageRepository();

		MonthPage theMostRecentMonth = pagesRepo.findMostRecentMonthPage();
		MonthPage tempMonth;

		do {
			processDate.add(Calendar.MONTH, 1);
			tempMonth = pagesRepo.
				findForDate(processDate.get(Calendar.YEAR),
							processDate.get(Calendar.MONTH));

			tempMonth.performBalanceCorrection(balanceCorrection);
			pagesRepo.save(tempMonth);

		} while ((theMostRecentMonth.getYear() != processDate.
			get(Calendar.YEAR))
			|| (theMostRecentMonth.getMonth() != processDate.
			get(Calendar.MONTH)));

	}

}
