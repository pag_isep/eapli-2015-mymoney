/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.exceptions;

import eapli.framework.domain.Money;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class InsufficientBalanceException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final Money currentBalance;
    private final Money expectedSpend;

    public InsufficientBalanceException(Money currentBalance,
                                        Money expectedSpend) {
        this.currentBalance = currentBalance;
        this.expectedSpend = expectedSpend;
    }

    /**
     * @return the currentBalance
     */
    public Money cCurrentBalance() {
        return currentBalance;
    }

    /**
     * @return the expectedSpend
     */
    public Money expectedSpend() {
        return expectedSpend;
    }
}
