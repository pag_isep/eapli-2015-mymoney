/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.limits;

import eapli.framework.domain.Money;
import eapli.framework.patterns.DomainEvent;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Should be specialized by any other kind of limit implementation.
 *
 * @author nuno
 */
@Entity
public abstract class AbstractLimit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	/**
	 * Yellow limit value which should be higher than Red Limit.
	 */
	@AttributeOverrides({
		@AttributeOverride(name = "amount", column = @Column(name = "yellow_amount")),
		@AttributeOverride(name = "currency", column = @Column(name = "yellow_curr"))
	})
	@Embedded
	private Money yellowLimit = null;

	/**
	 * Red Limit value which should be lower than Yellow Limit.
	 */
	@AttributeOverrides({
		@AttributeOverride(name = "amount", column = @Column(name = "red_amount")),
		@AttributeOverride(name = "currency", column = @Column(name = "red_curr"))
	})
	@Embedded
	private Money redLimit = null;

	protected AbstractLimit() {
		// for ORM tool
	}

	public AbstractLimit(final Money yellowLimit, final Money redLimit) {
		String exceptionMessage = null;
		if (yellowLimit == null
			|| redLimit == null
			|| redLimit.greaterThanOrEqual(yellowLimit)) {
			exceptionMessage = "Red Limit should be less than Yellow Limit";
			throw new IllegalArgumentException(exceptionMessage);
		} else {
			this.yellowLimit = yellowLimit;
			this.redLimit = redLimit;
		}
	}

	/**
	 * Returns the Yellow (higher) AbstractLimit Value.
	 *
	 * @return value for the yellow limit
	 */
	public Money getYellowLimit() {
		return yellowLimit;
	}

	/**
	 * Returns the Red (lower) AbstractLimit Value.
	 *
	 * @return value for the red limit
	 */
	public Money getRedLimit() {
		return redLimit;
	}

	/**
	 * Checks if any of the limits has been exceeded.
	 *
	 * @param expenseRegistered The Expense that fired the limit analysis.
	 * @return returns true if any limit has been exceeded, false otherwise
	 */
	public abstract boolean isLimitExceeded(DomainEvent expenseRegistered);

	/**
	 * Returns if this yellow limit flag has been exceeded.
	 *
	 * @param value Comparison value
	 * @return True if yellow limit has been exceeded, false otherwise
	 */
	public boolean isYellowLimitExceeded(Money value) {
		if (value == null) {
			throw new IllegalArgumentException("value is invalid");
		}
		return value.lessThan(getYellowLimit());
	}

	/**
	 * Returns if this red limit flag has been exceeded.
	 *
	 * @param value Comparison value
	 * @return True if Red limit has been exceeded, false otherwise
	 */
	public boolean isRedLimitExceeded(Money value) {
		if (value == null) {
			throw new IllegalArgumentException("value is invalid");
		}
		return value.lessThan(getRedLimit());
	}
}
