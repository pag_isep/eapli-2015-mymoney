/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.limits;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 *
 * @author mcn
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class AlertLimit implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    protected Long id;
    @Enumerated(EnumType.STRING)
    protected AlertLimitType alertType;

    protected AlertLimit() {
    }

    public AlertLimit(AlertLimitType alertType) {
        this.alertType = alertType;
    }

    public AlertLimitType getAlertType() {
        return alertType;
    }

    @Override
    public String toString() {
        String str = "Alert Type:" + alertType;
        return str;
    }

    public boolean hasId() {
        return (id != null);
    }

    /**
     * checks if the current object has the identity passed as a parameter
     *
     * @param id the identity to check
     * @return true if the object has the identity
     */
    public boolean is(Long id) {
        return this.id.equals(id);
    }
}
