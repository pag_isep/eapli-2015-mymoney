/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.limits;

import eapli.framework.patterns.DomainEvent;
import eapli.mymoney.domain.movements.ExpenseRegistered;
import eapli.mymoney.domain.limits.AbstractLimit;
import eapli.mymoney.domain.limits.BalanceLimit;

/**
 * This factory should handle the creation of Alert Events for the UI. Each
 * Alert Event specific creation based on the Event trigger is handled here .
 *
 * @author Nuno Bettencourt <nmb@isep.ipp.pt>
 */
public class AlertLimitEventFactory {

    /**
     * Returns an Alert Event for the given limit
     *
     * @param limit             Limit that has been exceeded
     * @param expenseRegistered
     * @return
     */
    public static DomainEvent buildAlertEvent(AbstractLimit limit,
                                              DomainEvent eventBase) {

        DomainEvent newEventBase = null;
        //TODO: remover daqui o instance of e passar para instanciação dinâmica

        if (limit instanceof BalanceLimit) {
            newEventBase = createLowBalanceEvent((BalanceLimit) limit, eventBase);
        }
        return newEventBase;

    }

    /**
     * Returns an Alert Event for the given limit
     *
     * @param limit     Specific limit that has been exceeded
     * @param eventBase Domain Event that had been fired
     * @return
     */
    public static DomainEvent createLowBalanceEvent(BalanceLimit limit,
                                                    DomainEvent eventBase) {
        ExpenseRegistered expenseRegistered;

        expenseRegistered = (ExpenseRegistered) eventBase;
        DomainEvent newEventBase = new LowBalanceLimitReachedEvent(expenseRegistered.
                theExpense().occurredOn());
        return newEventBase;
    }
}
