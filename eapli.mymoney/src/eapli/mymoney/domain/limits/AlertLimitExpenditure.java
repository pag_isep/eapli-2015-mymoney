/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.limits;

import eapli.framework.domain.Money;
import java.text.NumberFormat;
import java.util.Locale;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;

/**
 *
 * @author mcn
 */
@Entity
public class AlertLimitExpenditure extends AlertLimit {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @AttributeOverrides({
        @AttributeOverride(name = "amount", column = @Column(name = "yellow_amount")),
        @AttributeOverride(name = "currency", column = @Column(name = "yellow_curr"))
    })
    @Embedded
    private Money limitYellow;
    @AttributeOverrides({
        @AttributeOverride(name = "amount", column = @Column(name = "red_amount")),
        @AttributeOverride(name = "currency", column = @Column(name = "red_curr"))
    })
    @Embedded
    private Money limitRed;

    public AlertLimitExpenditure() {
    }

    public AlertLimitExpenditure(AlertLimitType alertType,
                                 Money limitYellow, Money limitRed) {
        super(alertType);
        this.limitYellow = limitYellow;
        this.limitRed = limitRed;
    }

    public Money getLimitYellow() {
        return limitYellow;
    }

    public Money getLimitRed() {
        return limitRed;
    }

//    public void setLimitYellow(Money limitYellow) {
//        this.limitYellow = limitYellow;
//    }
//
//    public void setLimitRed(Money limitRed) {
//        this.limitRed = limitRed;
//    }
    @Override
    public String toString() {
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.FRANCE);
        double doubleLimitYellow = this.limitYellow.amount();
        double doubleLimitRed = this.limitRed.amount();
        String str = super.toString() + "\nYellow Limit:"
                + n.format(doubleLimitYellow) + "\nRed Limit:"
                + n.format(doubleLimitRed);
        return str;
    }

    public void updateLimits(Money limitYellow, Money limitRed) {
        this.limitYellow = limitYellow;
        this.limitRed = limitRed;
    }
}
