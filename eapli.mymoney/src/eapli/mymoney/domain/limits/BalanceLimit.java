package eapli.mymoney.domain.limits;

import eapli.framework.domain.Money;
import eapli.framework.patterns.DomainEvent;
import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.Persistence;
import javax.persistence.Entity;

/**
 * Defines limit values over balance.
 *
 * @author nuno
 */
@Entity
public class BalanceLimit extends AbstractLimit {

    private BalanceLimit() {
        super();
    }

    /**
     * Balance AbstractLimit constructor.
     *
     * @param yellowLimit value for yellow limit, should be higher than red
     *                    limit
     * @param redLimit    value for red limit, should be lower than yellow limit
     */
    public BalanceLimit(final Money yellowLimit, final Money redLimit) {
        super(yellowLimit, redLimit);
    }

    /**
     * Blackbox to get current balance
     *
     * @return Current book balance
     */
    private Money currentBalance() {
//        final BookRepository repo = Persistence.getRepositoryFactory().
//                getBookRepository();
//
//        final Book theBook = repo.theBook();
//        return theBook.currentMonth().balance();
        final CheckingAccountRepository repo = Persistence.getRepositoryFactory().
                checkingAccountRepository();

        final CheckingAccount theBook = repo.theAccount();
        return theBook.balance();
    }

    /**
     * Returns
     *
     * @param expense
     * @return
     */
    @Override
    public boolean isLimitExceeded(DomainEvent eventRegistered) {
        if (eventRegistered == null) {
            throw new IllegalArgumentException("DomainEventBase is null");
        }

        Money currentBalance = currentBalance();

        return isRedLimitExceeded(currentBalance)
                || isYellowLimitExceeded(currentBalance);

    }
}
