/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.limits;

import eapli.framework.domain.Money;
import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * @author mcn
 */
public class ExpenditureOverLimitAlertEvent extends AlertEvent {

    private final Money yellow;
    private final Money red;

    public ExpenditureOverLimitAlertEvent(String alertTypeDescription,
                                          Money yellow, Money red,
                                          Money value, String level) {
        super(alertTypeDescription, value, level);
        this.yellow = yellow;
        this.red = red;
    }

    @Override
    public String toString() {
        NumberFormat n = NumberFormat.getCurrencyInstance(Locale.getDefault());
        double yellow1 = this.yellow.amount(); //doubleValue();
        double red1 = this.red.amount(); //doubleValue();

        return super.toString() + "\nLimit Yellow:" + n.format(yellow1) + "      Limit Red:" + n.
                format(red1);
    }
}
