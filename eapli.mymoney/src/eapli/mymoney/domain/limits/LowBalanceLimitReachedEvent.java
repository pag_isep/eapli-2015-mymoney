/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.limits;

import eapli.framework.patterns.helpers.DomainEventBase;
import eapli.mymoney.domain.movements.Expense;
import eapli.mymoney.domain.limits.BalanceLimit;
import java.util.Calendar;

/**
 *
 * @author Nuno Bettencourt <nmb@isep.ipp.pt>
 */
public class LowBalanceLimitReachedEvent extends DomainEventBase {
	// TODO shouldn't this class be named something like LowBalanceLimitReachedEvent?

	/**
	 * The expense object that originated the Alert.
	 */
	Expense theExpense;

	/**
	 * The Limit that was exceeded
	 */
	BalanceLimit theBalanceLimit;

	/**
	 *
	 * @param occuredAt
	 */
	public LowBalanceLimitReachedEvent(Calendar occuredAt) {
		super(occuredAt);
	}

	/**
	 *
	 * @return
	 */
	@Override
	public String toString() {
		return "BALANCE LIMIT EXCEEDED";
	}

}
