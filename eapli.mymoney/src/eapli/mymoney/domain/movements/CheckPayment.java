/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.movements;

import eapli.mymoney.domain.paymentmeans.PaymentMean;
import javax.persistence.Embeddable;

/**
 *
 * @author mcn
 */
@Embeddable
public class CheckPayment extends Payment {

    private String checkNumber;

    protected CheckPayment() {
    }

    public CheckPayment(PaymentMean mean, String checkNumber) {
        super(mean);
        this.checkNumber = checkNumber;
    }

    public String description() {
        return super.description() + " Check Number:" + checkNumber;
    }

}
