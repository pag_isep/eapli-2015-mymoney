/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.movements;

import eapli.mymoney.domain.paymentmeans.Cheque;
import javax.persistence.Embeddable;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Embeddable
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class ChequePayment extends Payment {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String chequeNumber;

    protected ChequePayment() {
    }

    public ChequePayment(Cheque method, String chequeNumber) {
        super(method);
        this.chequeNumber = chequeNumber;
    }
}
