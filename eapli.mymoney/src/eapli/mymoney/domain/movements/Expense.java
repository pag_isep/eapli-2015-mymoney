/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.movements;

import eapli.framework.domain.Money;
import eapli.util.DateTime;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Entity
public class Expense extends Movement {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @ManyToOne(cascade = CascadeType.MERGE)
    private ExpenseType type;
    @ManyToOne(cascade = CascadeType.MERGE)
    private Payment payment;

    protected Expense() {
    }

    public Expense(String description, Calendar dateOccurred,
                   Money amount, ExpenseType type, Payment payment,
                   String[] tags) {
        super(description, dateOccurred, amount, tags);
        init(type, payment);
    }

    public Expense(String description, Calendar dateOccurred,
                   Money amount, ExpenseType type, Payment payment) {
        super(description, dateOccurred, amount);
        init(type, payment);
    }

    public Expense(String description, int year, int month,
                   int day, Money amount, ExpenseType type, Payment payment) {
        this(description, DateTime.newCalendar(year, month, day), amount, type,
                payment);
    }

    private void init(ExpenseType type, Payment payment) {
        if (type == null || payment == null) {
            throw new IllegalArgumentException();
        }
        this.type = type;
        this.payment = payment;
    }

    public ExpenseType getExpenseType() {
        return type;
    }

    public Payment getPayment() {
        return payment;
    }
}
