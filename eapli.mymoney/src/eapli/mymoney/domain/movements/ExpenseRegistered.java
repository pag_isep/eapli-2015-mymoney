/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.movements;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseRegistered extends MovementRegistered {

    // TODO check if it makes sense to use domain objects or if we should use raw data in events
    private final Expense theExpense;

    public ExpenseRegistered(Expense anExpense) {
        super(anExpense.occurredOn());
        this.theExpense = anExpense;
    }

    /**
     * Return the Expense associated to the event
     *
     * @return Expense object
     */
    public Expense theExpense() {
        return theExpense;
    }
}
