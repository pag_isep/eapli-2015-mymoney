/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.movements;

import eapli.framework.domain.Money;
import java.util.Calendar;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Entity
public class Income extends Movement {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @ManyToOne(cascade = CascadeType.MERGE)
    private IncomeType type;

    protected Income() {
    }

    public Income(String description, Calendar dateOccurred, Money amount,
                  IncomeType type) {
        super(description, dateOccurred, amount);
        this.type = type;
    }

    public IncomeType getIncomeType() {
        return type;
    }
}
