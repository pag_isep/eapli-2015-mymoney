/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.movements;

import eapli.framework.domain.Money;
import eapli.framework.patterns.ValueObject;
import java.util.Calendar;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;

/**
 *
 * @author arocha
 */
@Embeddable
public class InitialBalance implements ValueObject {
    // TODO should this class be an inner class of the chekcingAccount instead of
    // a public class in the package

    @Temporal(javax.persistence.TemporalType.DATE)
    private Calendar referenceDate;
    // TODO - verify if there are movements before this date
    private Money initialValue;

    protected InitialBalance() {
    }

    public InitialBalance(Calendar startDate, Money howMuch) {
        if (howMuch == null || startDate == null
                || howMuch.lessThanOrEqual(Money.euros(0))) {
            throw new IllegalArgumentException();
        }
        this.initialValue = howMuch;
        this.referenceDate = startDate;
    }

    public Money value() {
        return initialValue;
    }

    public Calendar referenceDate() {
        return referenceDate;
    }
}
