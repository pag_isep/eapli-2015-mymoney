/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.movements;

import eapli.framework.patterns.DomainEvent;
import eapli.framework.patterns.helpers.DomainEventBase;
import java.util.Calendar;

/**
 *
 * @author Paulo Gandra Sousa
 */
public abstract class MovementRegistered extends DomainEventBase implements DomainEvent {

	public MovementRegistered(Calendar occurredAt) {
		super(occurredAt);
	}
}
