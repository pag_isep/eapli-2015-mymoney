/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.movements;

import eapli.mymoney.domain.paymentmeans.PaymentMean;
import eapli.framework.patterns.ValueObject;
import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.Inheritance;
import javax.persistence.OneToOne;

/**
 *
 * @author mcn
 */
@Embeddable
@Inheritance
public class Payment implements ValueObject {

//    // TODO should this class be public or should the Expense be the only one to have access to Payments?
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
    @OneToOne(cascade = CascadeType.REFRESH)
    private PaymentMean mean;

    public Payment(PaymentMean mean) {
        if (mean == null) {
            throw new IllegalArgumentException();
        }
        this.mean = mean;
    }

    protected Payment() {
        // for ORM tool only
    }

    public String description() {
        return mean.description();
    }
}
