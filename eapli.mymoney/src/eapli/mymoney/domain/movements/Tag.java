/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.movements;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Entity
public class Tag {
//TODO check if this shouldn't be a value object

    @Id
    @GeneratedValue
    private Long id;
    private String tag;

    protected Tag() {
    }

    public Tag(String tag) {
        this.tag = tag;
    }
}
