/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.paymentmeans;

import eapli.util.Validations;
import javax.persistence.Entity;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
@Entity
public class BankCheckBook extends PaymentMean {

    private String chequebookName;
    private String bankName;
    // TODO account number is not actually a "number" but a string of digits
    private String accountNumber;

    protected BankCheckBook() {
    }

    public BankCheckBook(String chequebookName, final String bankName,
                         final String accountNumber) {
        super();
        if (Validations.isNullOrEmpty(chequebookName) || Validations.isNullOrEmpty(bankName)) {
            throw new IllegalArgumentException();
        }

        //FIXME validate accountnumber
//		if (!Validations.isPositiveNumber(accountNumber)) {
//			throw new IllegalArgumentException();
//		}
        this.chequebookName = chequebookName;
        this.bankName = bankName;
        this.accountNumber = accountNumber;
    }

    @Override
    public String description() {
        return "BANK CHECK BOOK -Bank:" + bankName + " Account Number:" + accountNumber;
    }

}
