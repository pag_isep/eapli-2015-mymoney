/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.paymentmeans;

import eapli.mymoney.domain.paymentmeans.PaymentMean;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Cash extends PaymentMean {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static final String EUR = "EUR";

    private String currency;

    protected Cash() {
    }

    public Cash(String currency) {
        // may allow null if the currency is not important
        this.currency = currency;
    }

    @Override
    public String description() {
        return "Cash " + currency;
    }
}
