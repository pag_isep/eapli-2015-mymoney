/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.paymentmeans;

import eapli.util.Validations;
import java.util.Calendar;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Temporal;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class PaymentCard extends PaymentMean {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    protected String cardName;
    protected String nameOnCard;
    protected String bankName;
    @Temporal(javax.persistence.TemporalType.DATE)
    protected Calendar validUntil;
    //FIXME CardNumber is a good candidate for a Value Object (class)
    protected String cardNumber;

    public PaymentCard() {
    }

    public PaymentCard(final String bankName,
                       final Calendar validity,
                       final String cardNumber) {
        super();
        if (Validations.isNullOrEmpty(bankName) || Validations.
                isNullOrWhiteSpace(bankName)) {
            throw new IllegalArgumentException();
        }

        if (validity == null) {
            throw new IllegalArgumentException();
        }

//        if (!Validations.isPositiveNumber(cardNumber)) {
//            throw new IllegalArgumentException();
//        }
        this.bankName = bankName;
        this.validUntil = validity;
        this.cardNumber = cardNumber;
    }

    public String description() {
        return "Bank Name:" + this.bankName
                + " Card Number:" + this.cardNumber
                + " Validity: " + validUntil.get(Calendar.DAY_OF_MONTH) + "/"
                + validUntil.get(Calendar.MONTH) + "/"
                + validUntil.get(Calendar.YEAR);
    }

    public PaymentCard(String cardName, String bank, String cardNumber,
                       String nameOnCard, Calendar validUntil) {
        if (Validations.isNullOrEmpty(cardName) || Validations.
                isNullOrEmpty(bank)
                || Validations.isNullOrEmpty(cardNumber) || Validations.
                isNullOrEmpty(nameOnCard)
                || validUntil == null) {
            throw new IllegalArgumentException();
        }

        this.cardName = cardName;
        this.bankName = bank;
        this.cardNumber = cardNumber;
        this.nameOnCard = nameOnCard;
        this.validUntil = validUntil;
    }

}
