/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.paymentmeans;

import eapli.framework.domain.Identifiable;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
@Entity
@Inheritance
public abstract class PaymentMean implements Serializable, Identifiable<Long> {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    protected Long id;

    protected PaymentMean() {
        // for ORM tool
    }

    @Override
    public Long id() {
        return id;
    }

    @Override
    public boolean is(Long id) {
        return Objects.equals(this.id, id);
    }

    public abstract String description();

    @Override
    public boolean equals(Object other) {
        if (other == null) {
            return false;
        }
        if (this.getClass() != other.getClass()) {
            return false;
        }
        PaymentMean typedOther = (PaymentMean) other;
        return Objects.equals(this.id, typedOther.id);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }
}
