/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.savings;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.movements.Movement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

/**
 *
 * @author losa
 */
@Entity
public class SavingGoal implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    @Temporal(javax.persistence.TemporalType.DATE)
    // TODO what is the purpose of this field? the expected date of conclusion for the goal?
    // if so, the name should be more explicit
    private final Date ocurred = new Date();
    @OneToMany(cascade = CascadeType.ALL)
    private final List<Movement> savings = new ArrayList<Movement>();
    private String goal;
    @AttributeOverrides({
        @AttributeOverride(name = "amount", column = @Column(name = "target_amount")),
        @AttributeOverride(name = "currency", column = @Column(name = "target_curr"))
    })
    @Embedded
    private Money targetAmount;
    @AttributeOverrides({
        @AttributeOverride(name = "amount", column = @Column(name = "actual_amount")),
        @AttributeOverride(name = "currency", column = @Column(name = "actual_curr"))
    })
    @Embedded
    private Money actualSavings;

    public SavingGoal() {
    }

    public SavingGoal(String goal, Money targetAmount) {
        this.goal = goal;
        this.targetAmount = targetAmount;
        this.actualSavings = Money.euros(0);
    }

    public boolean registerSavingWithdraw(SavingWithdraw saving) {
        //return 1 if bigger
        if (saving.amount().compareTo(actualSavings) == 1) {
            // TODO should throw a busines exception instead of return true/false
            return false;
        }

        savings.add(saving);
        actualSavings = actualSavings.subtract(saving.amount());
        return true;
    }

    public void registerSavingDeposit(SavingDeposit saving) {
        savings.add(saving);
        actualSavings = actualSavings.add(saving.amount());
    }

    @Override
    public String toString() {
        //String str = goal + targetAmount.setScale(2) + getActualSavings().setScale(2) + ocurred;
        String str = goal + targetAmount + getActualSavings() + ocurred;
        return str;
    }

    //ajs p/ o visitor
    public String getDescription() {
        //String str = goal + " Target Amount:" + targetAmount.setScale(2) + " Actual Savings:" + getActualSavings().setScale(2);
        String str = goal + " Target Amount:" + targetAmount + " Actual Savings:" + getActualSavings();
        return str;
    }

    /**
     * @return the actualSavings
     */
    public Money getActualSavings() {
        return actualSavings;
    }

    // TODO this class works as goal and also as a tracking account; should be divided in two
    public boolean enoughSavings(Money amount) {
        // return 1 if bigger
        if (amount.compareTo(actualSavings) == 1) {
            return false;
        }

        return true;
    }

    /**
     * check if we already have the desired target amount
     *
     * @return
     */
    public boolean isGoalFulfiled() {
        return actualSavings.compareTo(targetAmount) >= 0;
    }
}
