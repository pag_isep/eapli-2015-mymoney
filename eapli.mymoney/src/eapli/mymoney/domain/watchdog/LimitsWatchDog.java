/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.watchdog;

import eapli.framework.domain.Money;
import eapli.framework.patterns.DomainEvent;
import eapli.mymoney.domain.limits.AlertLimitByExpenseType;
import eapli.mymoney.domain.limits.AlertLimitExpenditure;
import eapli.mymoney.domain.limits.AlertLimitType;
import eapli.mymoney.domain.CheckingAccount;
import eapli.mymoney.domain.movements.Expense;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.domain.movements.ExpenseRegistered;
import eapli.mymoney.domain.limits.AlertEvent;
import eapli.mymoney.domain.limits.ExpenditureByExpenseTypeOverLimitAlertEvent;
import eapli.mymoney.domain.limits.ExpenditureOverLimitAlertEvent;
import eapli.mymoney.persistence.AlertLimitRepository;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.Persistence;
import java.util.Calendar;
import java.util.Observable;

/**
 *
 * @author mcn
 */
public class LimitsWatchDog extends WatchDog {

    private void publish(DomainEvent eventBase) {
        setChanged();
        notifyObservers(eventBase);
    }

    /*
     * @Override
     * public void update(Observable o, Object arg) {
     * //TODO: NMB how to remove the following cast? (create innerclasses that
     * only handle this kind of event)
     * DomainEvent expenseRegistered = (DomainEvent) arg;
     *
     * LimitRepository limitRepository = Persistence.getRepositoryFactory().
     * getLimitRepository();
     *
     * // TODO considerar ter uma cache para não questionar constantemente o
     * repositório
     * // TODO uma vez que apenas temos um limite para saldo ( ver método
     * setBalanceLimit() ) porque não ir diretamente ao repositorio buscar esse
     * objeto - getBalanceLimit() - em vez de obter todos os limites?
     * // com os requisitos existentes não há multiplos limites
     * for (AbstractLimit limit : limitRepository.all()) {
     * if (limit.isLimitExceeded(expenseRegistered)) {
     * DomainEvent newEventBase = AlertLimitEventFactory.
     * buildAlertEvent(limit, expenseRegistered);
     * publish(newEventBase);
     * }
     * }
     * }
     */
    @Override
    public void update(Observable o, Object arg) {
        ExpenseRegistered expenseRegisteredEvent = (ExpenseRegistered) arg;
        verifyLimits(expenseRegisteredEvent);
    }

    private void verifyLimits(ExpenseRegistered event) {
        checkIfIsOverLimitWeekExpenditure(event.theExpense());
        checkIfIsOverLimitMonthExpenditure(event.theExpense());
        checkIfIsOverLimitDeviationByExpenseType(event.theExpense());
        checkIfIsOverBalanceLimit(event);
    }

    /**
     * Only publish the event ExpenseRegistred creates some events because was
     * considered the information expert
     */
    private void publishEvent(AlertEvent alert) {
        if (alert != null) {
            this.setChanged();
            notifyObservers(alert);
        }
    }

    private AlertEvent buildAlertBalanceEvent(AlertLimitExpenditure alertLimit,
                                              ExpenseRegistered event) {
        CheckingAccountRepository repo = Persistence.
                buildPersistence().checkingAccountRepository();
        CheckingAccount account = repo.theAccount();
        Money balance = account.balance();
        //Because the expense registered is not saved yet
        balance = balance.subtract(event.theExpense().amount());
        AlertEvent alert = buildAlertBalanceEvent(balance, alertLimit.
                getAlertType());
        return alert;
    }

    private ExpenditureOverLimitAlertEvent buildAlertBalanceEvent(
            Money balance,
            AlertLimitType alertType) {
        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        AlertLimitExpenditure alertLimit = (AlertLimitExpenditure) repo.
                findByAlertType(alertType);
        if (alertLimit != null) {
            if (balance.compareTo(alertLimit.getLimitRed()) < 0) {
                return new ExpenditureOverLimitAlertEvent(alertLimit.
                        getAlertType().
                        getDescription(),
                        alertLimit.
                        getLimitYellow(), alertLimit.
                        getLimitRed(), balance, "RED");
            }
            if (balance.compareTo(alertLimit.getLimitYellow()) < 0) {
                return new ExpenditureOverLimitAlertEvent(alertLimit.
                        getAlertType().
                        getDescription(),
                        alertLimit.
                        getLimitYellow(), alertLimit.
                        getLimitRed(), balance, "YELLOW");
            }
            return null;
        }
        return null;
    }

//    private int getYearOcurred() {
//        Date date = expenseRegistered.getDateOcurred();
//        return DateTime.dateToCalendar(date).get(Calendar.YEAR);
//    }
//
//    private int getMonthOccurred() {
//        Date date = expenseRegistered.getDateOcurred();
//        return DateTime.dateToCalendar(date).get(Calendar.MONTH) + 1;
//    }
//
//    private int getWeekOccurred() {
//        Date date = expenseRegistered.getDateOcurred();
//        int week = DateTime.weekNumber(date);
//        return week;
//    }

    /*
     * =========================
     */
    private void checkIfIsOverLimitWeekExpenditure(
            Expense expenseRegistered) {
        CheckingAccountRepository repo = Persistence
                .buildPersistence().checkingAccountRepository();
        CheckingAccount account = repo.theAccount();

        int year = expenseRegistered.occurredOn().get(Calendar.YEAR);
        int week = expenseRegistered.occurredOn().get(Calendar.WEEK_OF_YEAR);

        Money expenditure = account.expenditureOfWeek(year, week);

        // Because the event registered is not saved yet
        expenditure = expenditure.add(expenseRegistered.amount());
        AlertEvent alert = buildAlertEvent(expenditure,
                AlertLimitType.LIMIT_WEEK_EXPENDITURE);
        publishEvent(alert);
    }

    private void checkIfIsOverLimitMonthExpenditure(
            Expense expenseRegistered) {
        CheckingAccountRepository repo = Persistence
                .buildPersistence().checkingAccountRepository();
        CheckingAccount account = repo.theAccount();
        int year = expenseRegistered.occurredOn().get(Calendar.YEAR);
        int month = expenseRegistered.occurredOn().get(Calendar.MONTH);
        Money expenditure = account.expenditureOfMonth(year, month);
        // Because the event registered is not saved yet
        expenditure = expenditure.add(expenseRegistered.amount());
        AlertEvent alert = buildAlertEvent(expenditure,
                AlertLimitType.LIMIT_MONTH_EXPENDITURE);
        publishEvent(alert);
    }

    private ExpenditureOverLimitAlertEvent buildAlertEvent(
            Money expenditure, AlertLimitType alertType) {
        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        AlertLimitExpenditure alertLimit = (AlertLimitExpenditure) repo
                .findByAlertType(alertType);
        if (alertLimit != null) {
            if (expenditure.compareTo(alertLimit.getLimitRed()) > 0) {
                return new ExpenditureOverLimitAlertEvent(alertLimit.
                        getAlertType().getDescription(), alertLimit.
                        getLimitYellow(), alertLimit.getLimitRed(), expenditure, "RED");
            }
            if (expenditure.compareTo(alertLimit.getLimitYellow()) > 0) {
                return new ExpenditureOverLimitAlertEvent(alertLimit
                        .getAlertType().getDescription(),
                        alertLimit.
                        getLimitYellow(), alertLimit.getLimitRed(),
                        expenditure, "YELLOW");
            }
            return null;
        }
        return null;
    }

    private void checkIfIsOverLimitDeviationByExpenseType(
            Expense expenseRegistered) {
        AlertLimitRepository alertLimitRepo = Persistence
                .buildPersistence().alertLimitRepository();
        ExpenseType eT = expenseRegistered.getExpenseType();
        AlertLimitByExpenseType alertLimitET = (AlertLimitByExpenseType) alertLimitRepo.
                findByExpenseType(eT);
        if (alertLimitET != null) {
            CheckingAccountRepository repo = Persistence
                    .buildPersistence().checkingAccountRepository();
            CheckingAccount account = repo.theAccount();
            Money average = account.averageExpenditure(eT);
            AlertEvent alert = buildAlertEventDeviation(expenseRegistered.
                    amount(), average, alertLimitET);

            publishEvent(alert);

        }
    }

    private ExpenditureByExpenseTypeOverLimitAlertEvent buildAlertEventDeviation(
            Money amount, Money average,
            AlertLimitByExpenseType alertLimitET) {
        double yellowPercent = alertLimitET.getPercentLimitYellow();
        double redPercent = alertLimitET.getPercentLimitRed();
        Money yellow = average.multiply(yellowPercent);
        Money red = average.multiply(redPercent);
        Money limMinYellow = average.subtract(yellow);
        Money limMaxYellow = average.add(yellow);
        Money limMinRed = average.subtract(red);
        Money limMaxRed = average.add(red);

        if ((amount.compareTo(limMinRed) <= 0)
                || (amount.compareTo(limMaxRed) >= 0)) {
            return new ExpenditureByExpenseTypeOverLimitAlertEvent(alertLimitET
                    .getAlertType().getDescription(), yellowPercent,
                    redPercent, amount, average, "RED",
                    alertLimitET.
                    getExpenseType());
        }
        if ((amount.compareTo(limMinYellow) <= 0)
                || (amount.compareTo(limMaxYellow) >= 0)) {
            return new ExpenditureByExpenseTypeOverLimitAlertEvent(alertLimitET
                    .getAlertType().getDescription(), yellowPercent,
                    redPercent, amount, average, "YELLOW",
                    alertLimitET.
                    getExpenseType());
        }
        return null;
    }

    private void checkIfIsOverBalanceLimit(ExpenseRegistered event) {
        AlertLimitRepository repo = Persistence
                .buildPersistence().alertLimitRepository();
        AlertLimitExpenditure alertLimitBalance = (AlertLimitExpenditure) repo.
                findByAlertType(AlertLimitType.LIMIT_MINIMUM_BALANCE);
        if (alertLimitBalance != null) {
            AlertEvent alert = buildAlertBalanceEvent(alertLimitBalance, event);
            publishEvent(alert);
        }
    }
}
