/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.domain.watchdog;

import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author mcn
 */
public class WatchDogFactory {

    private static WatchDogFactory instance = null;

    public static WatchDogFactory instance() {
        if (instance == null) {
            instance = new WatchDogFactory();
        }
        return instance;
    }

    private WatchDogFactory() {
    }

    /**
     * Creates a WatchDog for Expenditure Limits
     *
     * @param observer Object that wished to receive WatchDog notifications
     * @return Object that handles Expenditure Limits
     */
    public LimitsWatchDog buildWatchDogLimits(Observer obs) {
        LimitsWatchDog watchDog = new LimitsWatchDog();
        watchDog.addObserver(obs);
        return watchDog;
    }

    public Observable buildWatchDogSavings(Observer obs) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
