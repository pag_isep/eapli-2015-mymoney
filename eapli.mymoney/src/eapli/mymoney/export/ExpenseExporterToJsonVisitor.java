package eapli.mymoney.export;

import eapli.framework.visitor.Visitor;
import eapli.mymoney.domain.movements.Expense;
import eapli.util.DateTime;
import eapli.util.Math;
import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author Paulo Gandra Sousa
 *
 */
class ExpenseExporterToJsonVisitor implements Visitor<Expense> {

    private final Writer writer;

    ExpenseExporterToJsonVisitor(Writer writer) {
        this.writer = writer;
    }

    @Override
    public void visit(Expense visited) {
		// TODO this code is getting too much information from the expense
        // should use builder pattern

        // TODO explore the use of JAX-B for exporting the object to XML/JSON
        try {
            writer.write("{ \n");
            writer.write("occuredAt : '");
            writer.write(DateTime.format(visited.occurredOn()));
            writer.write("',\n");
            writer.write("amount : ");
            //TODO export currency
            writer.write(Math.format(visited.amount().amountAsDecimal()));
            writer.write(",\n");
            writer.write("description : '");
            writer.write(visited.description());
            writer.write("',\n");
            writer.write("expenseType : '");
            writer.write(visited.getExpenseType().id());
            writer.write("',\n");
            writer.write("paymentMean : '");
            writer.write(visited.getPayment().description());
            writer.write("'\n}");
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void beforeVisiting(Expense visited) {
        // nothing to do
    }

    @Override
    public void afterVisiting(Expense visited) {
        try {
            // TODO check if we are geenrating bad formated JSON at the last
            // element of the array
            writer.write(",\n");
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
