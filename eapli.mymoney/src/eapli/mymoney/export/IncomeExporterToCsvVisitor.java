package eapli.mymoney.export;

import eapli.framework.visitor.Visitor;
import eapli.mymoney.domain.movements.Income;
import eapli.util.DateTime;
import eapli.util.Math;
import java.io.IOException;
import java.io.Writer;

/**
 *
 * @author Paulo Gandra Sousa
 *
 */
class IncomeExporterToCsvVisitor implements Visitor<Income> {

    private final Writer writer;

    IncomeExporterToCsvVisitor(Writer writer) {
        this.writer = writer;
    }

    @Override
    public void visit(Income visited) {
        // TODO this code is getting too much information from the expense
        // should use builder pattern
        try {
            writer.write("income, ");

            // FIXME this code block is duplicated with ExpenseExporter
            writer.write(DateTime.format(visited.occurredOn()));
            writer.write(",");
            //TODO export currency
            writer.write(Math.format(visited.amount().amountAsDecimal()));
            writer.write(",");
            writer.write(visited.description());
            writer.write(",");
            writer.write(visited.getIncomeType().id());

            writer.write(",");
            writer.write("\n");
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void beforeVisiting(Income visited) {
        // nothing to do
    }

    @Override
    public void afterVisiting(Income visited) {
        // nothing to do
    }
}
