package eapli.mymoney.export;

import eapli.framework.visitor.Visitor;
import eapli.mymoney.domain.movements.Income;
import eapli.util.DateTime;
import eapli.util.Math;
import java.io.IOException;
import java.io.Writer;

class IncomeExporterToXmlVisitor implements Visitor<Income> {

    private final Writer writer;

    IncomeExporterToXmlVisitor(Writer writer) {
        this.writer = writer;
    }

    @Override
    public void visit(Income visited) {
		// TODO this code is getting too much information from the expense
        // should use builder pattern

        // TODO explore the use of JAX-B for exporting the object to XML/JSON
        try {
            writer.write("<Income>\n");
            // FIXME the following code block is duplicated with ExpenseExporter
            writer.write("<OccuredAt>");
            writer.write(DateTime.format(visited.occurredOn()));
            writer.write("</OccuredAt>\n");
            writer.write("<Amount>");
            //TODO export currency
            writer.write(Math.format(visited.amount().amountAsDecimal()));
            writer.write("</Amount>\n");
            writer.write("<Description>");
            writer.write(visited.description());
            writer.write("</Description>\n");

            writer.write("<IncomeType>");
            writer.write(visited.getIncomeType().id());
            writer.write("</IncomeType>\n");
            writer.write("</Income>\n");
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void beforeVisiting(Income visited) {
        // nothing to do
    }

    @Override
    public void afterVisiting(Income visited) {
        // nothing to do
    }
}
