/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.framework.persistence.repositories.Repository;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.domain.limits.AlertLimit;
import eapli.mymoney.domain.limits.AlertLimitType;

/**
 *
 * @author mcn
 */
public interface AlertLimitRepository extends Repository<AlertLimit, Long> {

    /**
     * find a specific limit based on its Alert Type
     *
     * @param a
     * @return
     */
    AlertLimit findByAlertType(AlertLimitType a);

    /**
     * finds a specific limit based on the Expense Type
     *
     * @param eT
     * @return
     */
    AlertLimit findByExpenseType(ExpenseType eT);
}
