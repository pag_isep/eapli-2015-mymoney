/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.domain.Book;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Deprecated
public interface BookRepository {

    /**
     * reconstructs the single book available from the persistence store
     *
     * @return the book
     */
    Book theBook();

    /**
     * saves the book back to the persistence store
     *
     * @param theBook
     * @return
     */
    Book save(final Book theBook);
}
