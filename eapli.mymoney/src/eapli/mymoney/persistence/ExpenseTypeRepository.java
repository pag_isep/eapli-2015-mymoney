/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.framework.persistence.repositories.IterableRepository;
import eapli.mymoney.domain.movements.ExpenseType;

/**
 *
 * @author Paulo Gandra Sousa
 */
public interface ExpenseTypeRepository extends IterableRepository<ExpenseType, String> {
}
