/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.domain.movements.Income;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public interface IncomeRepository {

    Income save(Income income);

    List<Income> all();
}
