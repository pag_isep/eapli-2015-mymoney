/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.framework.persistence.repositories.Repository;
import eapli.mymoney.domain.movements.IncomeType;

/**
 *
 * @author Paulo Gandra Sousa
 */
public interface IncomeTypeRepository extends Repository<IncomeType, String> {
}
