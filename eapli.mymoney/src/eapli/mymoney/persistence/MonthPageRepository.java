/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.domain.MonthPage;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
@Deprecated
public interface MonthPageRepository {

    MonthPage findForDate(final Calendar reference);

    MonthPage findForDate(int year, int month);

    MonthPage findMostRecentMonthPage();

    MonthPage save(final MonthPage month);

    boolean add(final MonthPage month);

    List<MonthPage> all();
}
