/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.framework.persistence.repositories.Repository;
import eapli.mymoney.domain.paymentmeans.Cash;
import eapli.mymoney.domain.paymentmeans.PaymentMean;

/**
 * @author Paulo Gandra Sousa
 */
public interface PaymentMeanRepository extends Repository<PaymentMean, Long> {

    Cash getCash(String currency);

}
