/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.MyMoneySettings;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * provides easy access to the persistence layer
 *
 * works as a factory of factories
 *
 * @author Paulo Gandra Sousa
 */
public final class Persistence {

    // TODO deviamos considerar fazer o refactorign (rename) deste método
    // para factory() ou repositories()
    public static RepositoryFactory getRepositoryFactory() {
        // return new InMemoryRepositoryFactory();
        // return new JpaRepositoryFactory();

        String factoryClassName = MyMoneySettings.instance().
                getRepositoryFactory();
        try {
            return (RepositoryFactory) Class.forName(factoryClassName).
                    newInstance();
        }
        catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            //FIXME handle exception properly
            return null;
        }
    }

    public static eapli.mymoney.persistence.RepositoryFactory buildPersistence() {
        String factoryClassName = MyMoneySettings.instance().
                getApplicationProperties().
                getProperty("persistence.repositoryFactory");

        try {
            return (eapli.mymoney.persistence.RepositoryFactory) Class.forName(factoryClassName).
                    newInstance();
        }
        catch (ClassNotFoundException ex) {
            Logger.getLogger(eapli.mymoney.persistence.Persistence.class.getName()).
                    log(Level.SEVERE, null, ex);
            return null;
        }
        catch (InstantiationException ex) {
            Logger.getLogger(eapli.mymoney.persistence.Persistence.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex) {
            Logger.getLogger(eapli.mymoney.persistence.Persistence.class.getName()).
                    log(Level.SEVERE, null, ex);
            return null;
        }
        return null;
    }

    private Persistence() {
    }
}
