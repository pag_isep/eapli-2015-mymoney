/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

/**
 *
 * @author Paulo Gandra Sousa
 */
public interface RepositoryFactory {

    @Deprecated
    BookRepository getBookRepository();

    @Deprecated
    MonthPageRepository getMonthPageRepository();

    CheckingAccountRepository checkingAccountRepository();

    SavingPlanRepository savingPlanRepository();

    ExpenseRepository expenseRepository();

    ExpenseTypeRepository expenseTypeRepository();

    IncomeRepository incomeRepository();

    IncomeTypeRepository incomeTypeRepository();

    WalletRepository getWalletRepository();

    PaymentMeanRepository paymentMeanRepository();

    LimitRepository getLimitRepository();

    AlertLimitRepository alertLimitRepository();
}
