/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence;

import eapli.mymoney.domain.paymentmeans.Wallet;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public interface WalletRepository {

	Wallet theWallet();

	Wallet save(Wallet theWallet);
}
