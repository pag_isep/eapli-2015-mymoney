/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.limits.AlertLimit;
import eapli.mymoney.domain.limits.AlertLimitByExpenseType;
import eapli.mymoney.domain.limits.AlertLimitType;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.persistence.AlertLimitRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mcn
 */
public class AlertLimitRepositoryImpl extends InMemoryRepositoryBase<AlertLimit, Long> implements AlertLimitRepository {

    private static final List<AlertLimit> store = new ArrayList<AlertLimit>();

    @Override
    protected List<AlertLimit> getStaticStore() {
        return Collections.unmodifiableList(store);
    }

    @Override
    public AlertLimit findByAlertType(AlertLimitType a) {
        for (AlertLimit one : store) {
            if (one.getAlertType().equals(a)) {
                return one;
            }
        }
        return null;
    }

    @Override
    public AlertLimit findByExpenseType(ExpenseType eT) {
        for (AlertLimit one : store) {
            if (one instanceof AlertLimitByExpenseType) {
                AlertLimitByExpenseType aet = (AlertLimitByExpenseType) one;
                if (aet.getExpenseType().equals(eT)) {
                    return one;
                }
            }
        }
        return null;
    }

    @Override
    protected boolean matches(AlertLimit entity, Long id) {
        return entity.is(id);
    }
}
