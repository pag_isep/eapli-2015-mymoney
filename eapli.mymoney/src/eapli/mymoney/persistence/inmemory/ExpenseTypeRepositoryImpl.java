/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseTypeRepositoryImpl extends InMemoryRepositoryBase<ExpenseType, String> implements ExpenseTypeRepository {

    static List<ExpenseType> expenseTypes = new ArrayList<ExpenseType>();

    @Override
    protected List<ExpenseType> getStaticStore() {
        return Collections.unmodifiableList(expenseTypes);
    }

    @Override
    protected boolean matches(ExpenseType entity, String id) {
        return entity.is(id);
    }

    public boolean add(eapli.mymoney.domain.movements.ExpenseType expenseType) {
        if (expenseType == null) {
            throw new IllegalArgumentException();
        }
        if (getStaticStore().contains(expenseType)) {
            //TODO rever se deviamos ter outra exceção mais significativa
            throw new IllegalStateException();
        }
        return getStaticStore().add(expenseType);
    }

}
