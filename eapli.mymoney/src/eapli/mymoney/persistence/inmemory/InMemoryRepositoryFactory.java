/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.persistence.AlertLimitRepository;
import eapli.mymoney.persistence.BookRepository;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.ExpenseRepository;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.IncomeRepository;
import eapli.mymoney.persistence.IncomeTypeRepository;
import eapli.mymoney.persistence.MonthPageRepository;
import eapli.mymoney.persistence.PaymentMeanRepository;
import eapli.mymoney.persistence.RepositoryFactory;
import eapli.mymoney.persistence.SavingPlanRepository;
import eapli.mymoney.persistence.WalletRepository;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class InMemoryRepositoryFactory implements RepositoryFactory {

    @Override
    public BookRepository getBookRepository() {
        return new eapli.mymoney.persistence.inmemory.BookRepositoryImpl();
    }

    @Override
    public MonthPageRepository getMonthPageRepository() {
        return new eapli.mymoney.persistence.inmemory.MonthPageRepositoryImpl();
    }

    @Override
    public ExpenseTypeRepository expenseTypeRepository() {
        return new eapli.mymoney.persistence.inmemory.ExpenseTypeRepositoryImpl();
    }

    @Override
    public IncomeTypeRepository incomeTypeRepository() {
        return new eapli.mymoney.persistence.inmemory.IncomeTypeRepositoryImpl();
    }

    @Override
    public WalletRepository getWalletRepository() {
        return new eapli.mymoney.persistence.inmemory.WalletRepositoryImpl();
    }

    @Override
    public LimitRepositoryImpl getLimitRepository() {
        return new eapli.mymoney.persistence.inmemory.LimitRepositoryImpl();
    }

    @Override
    public ExpenseRepository expenseRepository() {
        return new eapli.mymoney.persistence.inmemory.ExpenseRepositoryImpl();
    }

    @Override
    public SavingPlanRepository savingPlanRepository() {
        return new eapli.mymoney.persistence.inmemory.SavingPlanRepositoryImpl();
    }

    @Override
    public IncomeRepository incomeRepository() {
        return new eapli.mymoney.persistence.inmemory.IncomeRepositoryImpl();
    }

    @Override
    public CheckingAccountRepository checkingAccountRepository() {
        return new eapli.mymoney.persistence.inmemory.CheckingAccountRepositoryImpl();
    }

    @Override
    public PaymentMeanRepository paymentMeanRepository() {
        return new eapli.mymoney.persistence.inmemory.PaymentMeanRepositoryImpl();
    }

    @Override
    public AlertLimitRepository alertLimitRepository() {
        return new eapli.mymoney.persistence.inmemory.AlertLimitRepositoryImpl();
    }

}
