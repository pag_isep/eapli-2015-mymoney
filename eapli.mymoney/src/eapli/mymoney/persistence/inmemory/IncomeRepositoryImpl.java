/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.movements.Income;
import eapli.mymoney.persistence.IncomeRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class IncomeRepositoryImpl extends InMemoryRepositoryBase<Income, Long> implements IncomeRepository {

    static List<Income> incomes = new ArrayList<Income>();

    @Override
    protected List<Income> getStaticStore() {
        return Collections.unmodifiableList(incomes);
    }

    @Override
    protected boolean matches(Income entity, Long id) {
        return entity.is(id);
    }
}
