/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.movements.IncomeType;
import eapli.mymoney.persistence.IncomeTypeRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class IncomeTypeRepositoryImpl extends InMemoryRepositoryBase<IncomeType, String> implements IncomeTypeRepository {

    static List<IncomeType> incomeTypes = new ArrayList<IncomeType>();

    @Override
    protected List<IncomeType> getStaticStore() {
        return Collections.unmodifiableList(incomeTypes);
    }

    @Override
    protected boolean matches(IncomeType entity, String id) {
        return entity.is(id);
    }

    public void add(eapli.mymoney.domain.movements.IncomeType incomeType) {
        if (incomeType == null) {
            throw new IllegalArgumentException();
        }
        if (getStaticStore().contains(incomeType)) {
            //TODO rever se deviamos ter outra exceção mais significativa
            throw new IllegalStateException();
        }
        getStaticStore().add(incomeType);
    }
}
