/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.MonthPage;
import eapli.mymoney.persistence.MonthPageRepository;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class MonthPageRepositoryImpl implements MonthPageRepository {

	private final static List<MonthPage> data = new ArrayList<MonthPage>();

	@Override
	public MonthPage findForDate(Calendar date) {
		if (date == null) {
			throw new IllegalArgumentException();
		}

		for (MonthPage page : data) {
			if (page.holds(date)) {
				return page;
			}
		}
		return null;
	}

	@Override
	public MonthPage save(MonthPage month) {
		if (month == null) {
			throw new IllegalArgumentException();
		}
		if (!data.contains(month)) {
			data.add(month);
		}
		return month;
	}

	@Override
	public List<MonthPage> all() {
		return Collections.unmodifiableList(data);
	}

	@Override
	public MonthPage findForDate(int year, int month) {
		for (MonthPage page : data) {
			if (page.isOf(year, month)) {
				return page;
			}
		}
		return null;
	}

	public MonthPage findMostRecentMonthPage() {
		List<MonthPage> allMonthPages = all();
		if (allMonthPages.size() > 0) {
			MonthPage latestMonthPage = allMonthPages.get(0);
			// FIXME avoid getYear/getMonth
			int latestYear = latestMonthPage.getYear();
			int latestMonth = latestMonthPage.getMonth();
			for (MonthPage monthPage : allMonthPages) {
				if (monthPage.getYear() > latestYear) {
					latestMonthPage = monthPage;
					// FIXME avoid getYear/getMonth
					latestYear = monthPage.getYear();
					latestMonth = monthPage.getMonth();
				} else if (monthPage.getYear() == latestYear) {
					if (monthPage.getMonth() > latestMonth) {
						latestMonthPage = monthPage;
						latestMonth = monthPage.getMonth();
					}
				}
			}
			return latestMonthPage;
		}
		return null;
	}

	@Override
	public boolean add(MonthPage month) {
		if (month == null) {
			throw new IllegalArgumentException();
		}
		return data.add(month);
	}
}
