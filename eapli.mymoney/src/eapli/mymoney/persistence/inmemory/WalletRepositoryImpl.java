/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.inmemory;

import eapli.mymoney.domain.paymentmeans.Wallet;
import eapli.mymoney.persistence.WalletRepository;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public class WalletRepositoryImpl implements WalletRepository {

	// singleton
	static Wallet instance = new Wallet();

	@Override
	public Wallet theWallet() {
		return instance;
	}

	@Override
	public Wallet save(final Wallet theWallet) {
		if (instance != theWallet) {
			throw new IllegalStateException();
		}
		return theWallet;
	}
}
