/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.limits.AlertLimit;
import eapli.mymoney.domain.limits.AlertLimitType;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.persistence.AlertLimitRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author mcn
 */
public class AlertLimitRepositoryImpl extends JpaRepository<AlertLimit, Long> implements AlertLimitRepository {

    @Override
    public AlertLimit findById(Long key) {
        return super.read(key);
    }

    @Override
    public AlertLimit findByAlertType(AlertLimitType a) {
        EntityManager em = entityManager();
        Query q = em
                .createQuery("SELECT e FROM AlertLimit e WHERE e.alertType = :aLertT");
        q.setParameter("aLertT", a);
        @SuppressWarnings("unchecked")
        List<AlertLimit> list = q.getResultList();
        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }

    @Override
    public AlertLimit findByExpenseType(ExpenseType eT) {

        EntityManager em = entityManager();
        Query q = em
                .createQuery("SELECT e FROM AlertLimitByExpenseType e WHERE e.expenseType= :eT");
        q.setParameter("eT", eT);
        @SuppressWarnings("unchecked")
        List<AlertLimit> list = q.getResultList();
        if (list.isEmpty()) {

            return null;
        }
        return list.get(0);
    }

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }
}
