/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.domain.Money;
import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.movements.Expense;
import eapli.mymoney.persistence.ExpenseRepository;
import eapli.util.DateTime;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseRepositoryImpl extends JpaRepository<Expense, Long>
        implements ExpenseRepository {

    /**
     * gets the expenditure of a specific week
     *
     * note that this method is actually placing the business logic in the HQL
     * code which is normally something to avoid
     *
     * @param year
     * @param weekNumber
     * @return
     */
    @Override
    public Money expenditureOfWeek(int year, int weekNumber) {
        EntityManager em = entityManager();
        Query q = em
                .createQuery("SELECT SUM(e.amount) FROM Expense e WHERE e.dateOcurred >= :start AND e.dateOcurred <= :end");
        Date start = DateTime.beginningOfWeek(year, weekNumber).getTime();
        q.setParameter("start", start);
        Date end = DateTime.endOfWeek(year, weekNumber).getTime();
        q.setParameter("end", end);

        //FIXME use currency
        BigInteger balance = (BigInteger) q.getSingleResult();
        if (balance == null) {
            balance = BigInteger.ZERO;
        }
        return Money.euros(balance.doubleValue());
    }

    /**
     * gets the total expenditure of a specific month
     *
     * note that this method is actually placing the business logic in the HQL
     * code which is normally something to avoid
     *
     * @param year
     * @param month
     * @return
     */
    @Override
    public Money expenditureOfMonth(int year, int month) {
        EntityManager em = entityManager();
        Query q = em
                .createQuery("SELECT SUM(e.amount.amount) FROM Expense e WHERE e.occurred >= :start AND e.occurred <= :end");
        Date start = new Date(year - 1900, month - 1, 1);
        // Calendar start = DateTime.newCalendar(year, month, 1);
        q.setParameter("start", start);
        Date end = new Date(year - 1900, month - 1, 31);
        // Calendar end = DateTime.newCalendar(year, month, 31);
        q.setParameter("end", end);

        //FIXME use currency
        BigInteger balance = (BigInteger) q.getSingleResult();
        if (balance == null) {
            balance = BigInteger.ZERO;
        }
        return Money.euros(balance.doubleValue());
    }

    /**
     * gets the total amount of expenses
     *
     * note that this method is actualy placing the business logic in the HQL
     * code which is normally something to avoid
     *
     * @return
     */
    @Override
    public Money totalExpenditure() {
        EntityManager em = entityManager();
        Query q = em.createQuery("SELECT SUM(e.amount) FROM Expense e");

        //FIXME use currency
        BigInteger balance = (BigInteger) q.getSingleResult();
        if (balance == null) {
            balance = BigInteger.ZERO;
        }
        return Money.euros(balance.doubleValue());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Expense> between(Date start, Date end) {
        EntityManager em = entityManager();
        Query q = em
                .createQuery("SELECT e FROM Expense e WHERE e.ocurred >= :start AND e.ocurred <= :end");
        q.setParameter("start", start);
        q.setParameter("end", end);

        return q.getResultList();
    }

    @Override
    public List<Expense> getExpensesByTag(String tag) {
        EntityManager em = entityManager();
        Query q = em
                .createQuery("SELECT e FROM Expense e JOIN e.tags t WHERE t.tag = :tag");
        q.setParameter("tag", tag);

        return q.getResultList();
    }

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }
}
