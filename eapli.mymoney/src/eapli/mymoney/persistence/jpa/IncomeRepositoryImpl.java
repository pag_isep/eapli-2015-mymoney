/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.movements.Income;
import eapli.mymoney.persistence.IncomeRepository;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class IncomeRepositoryImpl extends JpaRepository<Income, Long> implements
        IncomeRepository {

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

}
