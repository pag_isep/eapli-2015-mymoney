/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.movements.IncomeType;
import eapli.mymoney.persistence.IncomeTypeRepository;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class IncomeTypeRepositoryImpl extends JpaRepository<IncomeType, String>
        implements IncomeTypeRepository {

    @Override
    public IncomeType findById(String shortDescription) {
        return super.read(shortDescription);
    }

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

}
