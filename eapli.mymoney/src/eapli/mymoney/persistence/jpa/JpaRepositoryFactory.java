/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.mymoney.persistence.AlertLimitRepository;
import eapli.mymoney.persistence.BookRepository;
import eapli.mymoney.persistence.CheckingAccountRepository;
import eapli.mymoney.persistence.ExpenseRepository;
import eapli.mymoney.persistence.ExpenseTypeRepository;
import eapli.mymoney.persistence.IncomeRepository;
import eapli.mymoney.persistence.IncomeTypeRepository;
import eapli.mymoney.persistence.LimitRepository;
import eapli.mymoney.persistence.MonthPageRepository;
import eapli.mymoney.persistence.PaymentMeanRepository;
import eapli.mymoney.persistence.RepositoryFactory;
import eapli.mymoney.persistence.SavingPlanRepository;
import eapli.mymoney.persistence.WalletRepository;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class JpaRepositoryFactory implements RepositoryFactory {

    @Override
    public BookRepository getBookRepository() {
        return new BookRepositoryImpl();
    }

    @Override
    public MonthPageRepository getMonthPageRepository() {
        return new MonthPageRepositoryImpl();
    }

    @Override
    public ExpenseTypeRepository expenseTypeRepository() {
        return new eapli.mymoney.persistence.jpa.ExpenseTypeRepositoryImpl();
    }

    @Override
    public IncomeTypeRepository incomeTypeRepository() {
        return new IncomeTypeRepositoryImpl();
    }

    @Override
    public WalletRepository getWalletRepository() {
        return new eapli.mymoney.persistence.jpa.WalletRepositoryImpl();
    }

    @Override
    public LimitRepository getLimitRepository() {
        return new LimitRepositoryImpl();
    }

    @Override
    public CheckingAccountRepository checkingAccountRepository() {
        return new CheckingAccountRepositoryImpl();
    }

    @Override
    public SavingPlanRepository savingPlanRepository() {
        return new SavingPlanRepositoryImpl();
    }

    @Override
    public ExpenseRepository expenseRepository() {
        return new ExpenseRepositoryImpl();
    }

    @Override
    public IncomeRepository incomeRepository() {
        return new IncomeRepositoryImpl();
    }

    @Override
    public PaymentMeanRepository paymentMeanRepository() {
        return new PaymentMeanRepositoryImpl();
    }

    @Override
    public AlertLimitRepository alertLimitRepository() {
        return new AlertLimitRepositoryImpl();
    }
}
