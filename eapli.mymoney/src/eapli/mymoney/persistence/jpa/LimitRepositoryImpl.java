/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.limits.AbstractLimit;
import eapli.mymoney.domain.limits.BalanceLimit;
import eapli.mymoney.persistence.LimitRepository;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class LimitRepositoryImpl implements LimitRepository {

	@Override
	public List<AbstractLimit> all() {
		return repoImpl.all();
	}

	private class RepoImpl extends JpaRepository<AbstractLimit, Long> {

		@Override
		protected String persistenceUnitName() {
			return PersistenceSettings.PERSISTENCE_UNIT_NAME;
		}

	}

	private RepoImpl repoImpl = new RepoImpl();

	@Override
	public void setBalanceLimit(BalanceLimit limit) {
		repoImpl.save(limit);
	}
}
