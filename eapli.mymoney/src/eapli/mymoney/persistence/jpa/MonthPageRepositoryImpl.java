/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.MonthPage;
import eapli.mymoney.persistence.MonthPageRepository;
import java.util.Calendar;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class MonthPageRepositoryImpl
	extends JpaRepository<MonthPage, Long>
	implements MonthPageRepository {

	@Override
	public MonthPage findForDate(Calendar date) {
		if (date == null) {
			throw new IllegalArgumentException();
		}
		return findForDate(date.get(Calendar.YEAR), date.get(Calendar.MONTH) + 1);
	}

	public MonthPage findForDate(int year, int month) {
		Query query = entityManager().
			createQuery("from MonthPage m where m.id.theMonth= :pmonth and m.id.theYear=:pyear");
		query.setParameter("pmonth", month);
		query.setParameter("pyear", year);
		try {
			MonthPage result = (MonthPage) query.getSingleResult();
			return result;
		} catch (NoResultException ex) {
			return null;
		}
	}

	@Override
	public MonthPage findMostRecentMonthPage() {
		Query q = entityManager().
			createQuery("select m from MonthPage m order by m.id.theYear desc, m.id.theMonth desc");
		q.setMaxResults(0);
		if (q.getResultList().size() == 0) {
			return null;
		}
		return (MonthPage) q.getResultList().get(0);
	}

//	public MonthPage findMostRecentMonthPage() {
//		List<MonthPage> allMonthPages = all();
//		if (allMonthPages.size() > 0) {
//			MonthPage latestMonthPage = allMonthPages.get(0);
//			// FIXME avoid getYear/getMonth
//			int latestYear = latestMonthPage.getYear();
//			int latestMonth = latestMonthPage.getMonth();
//			for (MonthPage monthPage : allMonthPages) {
//				if (monthPage.getYear() > latestYear) {
//					latestMonthPage = monthPage;
//					// FIXME avoid getYear/getMonth
//					latestYear = monthPage.getYear();
//					latestMonth = monthPage.getMonth();
//				} else if (monthPage.getYear() == latestYear) {
//					if (monthPage.getMonth() > latestMonth) {
//						latestMonthPage = monthPage;
//						latestMonth = monthPage.getMonth();
//					}
//				}
//			}
//			return latestMonthPage;
//		}
//		return null;
//	}
	@Override
	protected String persistenceUnitName() {
		return PersistenceSettings.PERSISTENCE_UNIT_NAME;
	}
}
