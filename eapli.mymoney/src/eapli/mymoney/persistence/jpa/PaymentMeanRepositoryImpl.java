/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.paymentmeans.Cash;
import eapli.mymoney.domain.paymentmeans.PaymentMean;
import eapli.mymoney.persistence.PaymentMeanRepository;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class PaymentMeanRepositoryImpl extends JpaRepository<PaymentMean, Long>
        implements PaymentMeanRepository {

    @Override
    public Cash getCash(String currency) {
        try {
            Query query = entityManager().createQuery(
                    "SELECT c FROM Cash c WHERE c.currency = :curr");
            query.setParameter("curr", currency);
            Cash cash = (Cash) query.getSingleResult();
            return cash;
        }
        catch (NoResultException ex) {
            //throw new IllegalStateException(ex);
            //TODO we are taking a risk here as to create the cash on demand but this should never happen. only to support bootstrapping
            return createCash(currency);
        }
    }

    @Override
    protected String persistenceUnitName() {
        return PersistenceSettings.PERSISTENCE_UNIT_NAME;
    }

    private Cash createCash(String currency) {
        Cash eur = new Cash(currency);
        return (Cash) save(eur);
    }
}
