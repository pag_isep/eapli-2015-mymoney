/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.persistence.jpa;

import eapli.framework.persistence.jpa.JpaRepository;
import eapli.mymoney.domain.paymentmeans.Wallet;
import eapli.mymoney.persistence.WalletRepository;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public class WalletRepositoryImpl extends JpaRepository<Wallet, Long> implements WalletRepository {

	@Override
	public Wallet theWallet() {
		Wallet theWallet = first();
		if (theWallet == null) {
			theWallet = new Wallet();
			add(theWallet);
		}
		return theWallet;
	}

	@Override
	protected String persistenceUnitName() {
		return PersistenceSettings.PERSISTENCE_UNIT_NAME;
	}
}
