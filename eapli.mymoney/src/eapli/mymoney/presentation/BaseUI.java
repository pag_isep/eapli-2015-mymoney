/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.patterns.helpers.DomainEventBase;
import eapli.framework.presentation.AbstractUI;
import eapli.mymoney.application.BaseController;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author PAG
 */
public abstract class BaseUI extends AbstractUI {

    public boolean show() {
        drawFormTitle();
        final boolean wantsToExit = doShow();
        showBalances();

        return wantsToExit;
    }

    public void showBalances() {
        drawFormBorder();
        drawFormTitle("Balances: ");
        NumberFormat formater = NumberFormat.getCurrencyInstance(Locale.FRANCE);
        double monthExpenditure = ((BaseController) controller()).getCurrentMonthExpenditure().
                amount();
        System.out.
                println("Current Month expenditure: " + formater.
                        format(monthExpenditure));

        //double weekExpenditure = controller().getThisWeekExpenditure();
        //System.out.println("Weekly expenditure" + nF.format(weekExpenditure));
        double balance = ((BaseController) controller()).getBalance().amount();
        System.out.
                println("Current balance: " + formater.
                        format(balance));

        drawFormBorder();
    }

    /**
     * AlertLimitObserver is only responsible for showing AlertLimits Messages.
     */
    protected class AlertLimitObserver implements Observer {

        @Override
        public void update(Observable o, Object arg) {
            DomainEventBase domainEventBase = (DomainEventBase) arg;
            displayAlert(domainEventBase);
        }

        public void displayAlert(DomainEventBase eventBase) {
            System.out.println(eventBase.toString());

        }
    }
}
