/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.framework.presentation.ListWidget;
import eapli.mymoney.application.ListExpenseTypesController;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.presentation.visitors.ExpenseTypeListVisitor;

/**
 *
 * @author Paulo Gandra Sousa
 */
class ListExpenseTypesUI extends BaseUI {

    private final ListExpenseTypesController controller = new ListExpenseTypesController();
    private ListWidget<ExpenseType> widget;

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    public boolean doShow() {
        widget = new ListWidget<ExpenseType>(controller.getAllExpenseTypes(), new ExpenseTypeListVisitor());
        widget.show();
        return true;
    }

    @Override
    public String headline() {
        return "LIST EXPENSE TYPES";
    }
}
