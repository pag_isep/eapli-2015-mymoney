/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.mymoney.application.ListExpensesController;
import eapli.mymoney.domain.movements.Expense;
import eapli.util.Console;
import java.util.List;

/**
 *
 * @author Fernando
 */
public class ListExpensesOfMonthUI extends BaseUI {

    private final ListExpensesController theController = new ListExpensesController();

    @Override
    protected Controller controller() {
        return theController;
    }

    @Override
    protected boolean doShow() {
        int year = Console.readInteger("What Year? = ");
        int month = Console.readInteger("What Month? = ");
        final List<Expense> expenses = theController.getExpensesOfMonth(year, month);

        for (Expense expense : expenses) {
            // FIXME não usar toString() para imprimir objetos
            System.out.println(expense);
        }
        return true;
    }

    @Override
    public String headline() {
        return "List Expenses";
    }
}
