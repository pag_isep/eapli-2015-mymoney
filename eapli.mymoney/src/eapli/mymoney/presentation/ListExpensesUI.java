/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.framework.presentation.ListWidget;
import eapli.mymoney.application.ListExpensesController;
import eapli.mymoney.domain.movements.Expense;
import eapli.mymoney.presentation.visitors.ExpenseListVisitor;
import eapli.util.Console;

/**
 *
 * @author Paulo Gandra Sousa
 */
class ListExpensesUI extends BaseUI {

    private final ListExpensesController controller = new ListExpensesController();

    ListWidget<Expense> widget;

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    public boolean doShow() {
        boolean searchByTag = Console.readBoolean("Search by a specific tag?");
        if (searchByTag) {
            String tag = Console.readLine("Tag:");
            widget = new ListWidget<Expense>(controller.getExpensesByTag(tag),
                    new ExpenseListVisitor());
        }
        else {
            widget = new ListWidget<Expense>(controller.getExpenses(),
                    new ExpenseListVisitor());
        }
        widget.show();

        return true;
    }

    @Override
    public String headline() {
        return "LIST EXPENSES";
    }
}
