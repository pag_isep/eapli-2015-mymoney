/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.framework.presentation.ListWidget;
import eapli.mymoney.application.ListIncomeTypesController;
import eapli.mymoney.domain.movements.IncomeType;
import eapli.mymoney.presentation.visitors.IncomeTypeListVisitor;

/**
 *
 * @author Paulo Gandra Sousa
 */
class ListIncomeTypesUI extends BaseUI {

    private final ListIncomeTypesController controller = new ListIncomeTypesController();
    private ListWidget<IncomeType> widget;

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    public boolean doShow() {
        widget = new ListWidget<IncomeType>(controller.getIncomeTypes(), new IncomeTypeListVisitor());
        widget.show();

        return true;
    }

    @Override
    public String headline() {
        return "LIST INCOME TYPES";
    }
}
