/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.framework.presentation.ListWidget;
import eapli.mymoney.application.ListIncomesController;
import eapli.mymoney.domain.movements.Income;
import eapli.mymoney.presentation.visitors.IncomeListVisitor;

/**
 *
 * @author Paulo Gandra Sousa
 */
class ListIncomesUI extends BaseUI {

    private final ListIncomesController controller = new ListIncomesController();
    private ListWidget<Income> widget;

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    protected boolean doShow() {
        widget = new ListWidget<Income>(controller.getIncomes(), new IncomeListVisitor());
        widget.show();

        return true;
    }

    @Override
    public String headline() {
        return "LIST INCOMES";
    }
}
