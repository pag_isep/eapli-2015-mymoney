/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.framework.presentation.ListWidget;
import eapli.mymoney.application.ListPaymentMeansController;
import eapli.mymoney.domain.paymentmeans.PaymentMean;
import eapli.mymoney.presentation.visitors.PaymentMeanListVisitor;

/**
 *
 * @author Paulo Gandra Sousa
 */
class ListPaymentMeansUI extends BaseUI {

    private final ListPaymentMeansController controller = new ListPaymentMeansController();
    private ListWidget<PaymentMean> widget;

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    protected boolean doShow() {
        widget = new ListWidget<PaymentMean>(controller.getPaymentMeans(), new PaymentMeanListVisitor());
        widget.show();

        return true;
    }

    @Override
    public String headline() {
        return "LIST PAYMENT METHODS";
    }
}
