/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.framework.domain.Money;
import eapli.mymoney.application.RegisterBalanceLimitController;
import eapli.util.Console;

/**
 *
 * @author nuno
 */
public class RegisterBalanceLimitUI extends BaseUI {

    /**
     * The Controller responsible for controlling set balance limit use case.
     */
    private final RegisterBalanceLimitController theController = new RegisterBalanceLimitController();

    /**
     * Temporary holder for Balance Yellow Limit.
     */
    private Money yellowLimit;

    /**
     * Temporary holder for Balance Red Limit.
     */
    private Money redLimit;

    @Override
    public boolean doShow() {
        double inputRedLimit;
        double inputYellowLimit;
        boolean success = false;

        while (!success) {
            try {

                inputRedLimit = Console.readDouble("Enter Red Balance Limit » ");
                inputYellowLimit = Console.
                        readDouble("Enter Yellow Balance Limit » ");

                setRedLimit(inputRedLimit);
                setYellowLimit(inputYellowLimit);

                submit();
                success = true;
            }
            catch (IllegalArgumentException e) {
                success = false;
                System.out.println(e.getMessage());
            }
        }
        return true;
    }

    /**
     * Sets a Red limit from a double value.
     *
     * @param yellowLimit The double yellow limit value to be converted
     */
    protected void setYellowLimit(double yellowLimit) {
        this.yellowLimit = Money.euros(yellowLimit);
    }

    /**
     * Sets a Red limit from a double value.
     *
     * @param redLimit The double yellow limit value to be converted
     */
    protected void setRedLimit(double redLimit) {
        this.redLimit = Money.euros(redLimit);
    }

    /**
     * Submits the request of adding a new Balance Limit to the theController.
     */
    public void submit() {
        //ASK: should the theController only be created here?
        theController.setYellowLimit(yellowLimit);
        theController.setRedLimit(redLimit);
        theController.submit();

        System.out.println("\nBalance Limit added!");
    }

    @Override
    protected Controller controller() {
        return theController;
    }

    @Override
    public String headline() {
        return "Add Balance Limits";
    }
}
