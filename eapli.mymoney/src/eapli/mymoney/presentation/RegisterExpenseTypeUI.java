/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.mymoney.application.RegisterExpenseTypeController;

/**
 *
 * @author arocha
 */
public class RegisterExpenseTypeUI extends RegisterMovementTypeBaseUI {

    private final RegisterExpenseTypeController controller = new RegisterExpenseTypeController();

    @Override
    public boolean doShow() {
        readGeneralData();

        controller.registerExpenseType(shortName, descr);

        System.out.println("\nExpense type recorded!");

        return true;
    }

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    public String headline() {
        return "REGISTER AN EXPENSE TYPE";
    }
}
