/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.framework.presentation.SelectWidget;
import eapli.mymoney.application.RegisterExpenseController;
import eapli.mymoney.domain.paymentmeans.Cheque;
import eapli.mymoney.domain.movements.ExpenseType;
import eapli.mymoney.domain.movements.Payment;
import eapli.mymoney.domain.paymentmeans.PaymentMean;
import eapli.mymoney.domain.exceptions.InsufficientBalanceException;
import eapli.mymoney.domain.limits.AlertEvent;
import eapli.mymoney.presentation.visitors.ExpenseTypeListVisitor;
import eapli.util.Console;
import eapli.util.Validations;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 *
 * @author Paulo Gandra Sousa
 */
class RegisterExpenseUI extends RegisterMovementBaseUI implements Observer {

    private SelectWidget<ExpenseType> expenseTypesSelectWidget;
    private final RegisterExpenseController controller = new RegisterExpenseController();

    @Override
    public String headline() {
        return "REGISTER AN EXPENSE";
    }

    @Override
    public boolean doShow() {
        readGeneralData();

        ExpenseType expenseType = readExpenseType();
        if (expenseType == null) {
            return true;
        }

        // TODO should the UI create a Payment object?
        Payment method = readPaymentMean();
        if (method == null) {
            return true;
        }

        // Delegate in controller my register as Observador Limits in this
        // process
        controller.addObserverRegisterExpense(this);
        try {
            controller.registerExpense(what, date, amount, expenseType, method);
            System.out.println("\nExpense recorded!");
        }
        catch (InsufficientBalanceException ex) {
            System.out
                    .println("!!!>>> Unable to register expense due to insufficient balance");
        }
        return true;
    }

    @Override
    protected Controller controller() {
        return controller;
    }

    private ExpenseType readExpenseType() {
        System.out.println("-- EXPENSE TYPES --");
        List<ExpenseType> listExpenseTypes = controller.getExpenseTypes();

        expenseTypesSelectWidget = new SelectWidget<ExpenseType>(
                listExpenseTypes, new ExpenseTypeListVisitor());
        expenseTypesSelectWidget.show();
        int option = expenseTypesSelectWidget.selectedOption();
        if (option == 0) {
            return null;
        }
        ExpenseType expenseType = listExpenseTypes.get(option - 1);
        return expenseType;
    }

    private Payment readPaymentMean() {
        System.out.println("-- PAYMENT MEANS --");
        List<PaymentMean> paymentMeans = controller.getPaymentMeans();
        // FIX use ListWidget to avoid code duplication
        int position = 1;
        for (PaymentMean method : paymentMeans) {
            System.out.println(position + ". " + method.description());
            position++;
        }
        System.out.println("0. Exit");
        final int option = Console.readOption(1, paymentMeans.size(), 0);
        if (option == 0) {
            return null;
        }
        PaymentMean method = paymentMeans.get(option - 1);

        // TODO should the UI ask for a Payment object or call different
        // registerXXX paymentMeans on the getController, i.e.,
        // registerExpensePaidwithCheque
        // or registerExpensePaidWithCash, etc...
        Payment payment;
        if (method.getClass() == Cheque.class) {
            final String chequeNumber = Console.readLine("Cheque number:");
            payment = controller.createChequePayment((Cheque) method,
                    chequeNumber);
        }
        else {
            payment = controller.createPayment(method);
        }

        return payment;
    }

    private String readValidEntry(String str) {
        String entry;
        boolean notValid;
        do {
            entry = Console.readLine(str);
            notValid = Validations.isNullOrWhiteSpace(entry);
            if (notValid) {
                System.out.println("Can not be null or white space. Repeat");
            }
        } while (notValid);
        return entry;
    }

    private double readValidAmount(String str) {
        double value;
        do {
            value = Console.readDouble(str);
            if (value <= 0) {
                System.out.println("The cost must be greater than 0.Repeat");
            }
        } while (value <= 0);
        return value;
    }

    // Observer pattern
    @Override
    public void update(final Observable sender, final Object arg) {
        AlertEvent al = (AlertEvent) arg;
        System.out
                .println("************************************ALERT*****************************");
        System.out.println(al);
        System.out
                .println("**********************************************************************");

    }
}
