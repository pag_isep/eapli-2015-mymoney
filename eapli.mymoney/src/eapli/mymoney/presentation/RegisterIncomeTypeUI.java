/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.mymoney.application.RegisterIncomeTypeController;

/**
 *
 * @author Paulo Gandra de Sousa
 */
public class RegisterIncomeTypeUI extends RegisterMovementTypeBaseUI {

    private final RegisterIncomeTypeController controller = new RegisterIncomeTypeController();

    @Override
    public boolean doShow() {
        readGeneralData();

        controller.registerIncomeType(shortName, descr);

        System.out.println("\nIncome type recorded!");

        return true;
    }

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    public String headline() {
        return "REGISTER AN EXPENSE TYPE";
    }
}
