/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.framework.presentation.SelectWidget;
import eapli.mymoney.application.RegisterIncomeController;
import eapli.mymoney.domain.movements.IncomeType;
import eapli.mymoney.presentation.visitors.IncomeTypeListVisitor;
import java.util.List;

/**
 *
 * @author Paulo Gandra Sousa
 */
class RegisterIncomeUI extends RegisterMovementBaseUI {

    private final RegisterIncomeController controller = new RegisterIncomeController();
    private SelectWidget<IncomeType> incomeTypesSelectWidget;

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    public boolean doShow() {
        readGeneralData();

        IncomeType incomeType = readIncomeType();
        if (incomeType == null) {
            return true;
        }

        controller.registerIncome(what, date, amount, incomeType);

        System.out.println("\nIncome recorded!");

        return true;
    }

    IncomeType readIncomeType() {
        System.out.println("-- INCOME TYPES --");
        List<IncomeType> listIncomeTypes = controller.getIncomeTypes();
        incomeTypesSelectWidget = new SelectWidget<IncomeType>(listIncomeTypes, new IncomeTypeListVisitor());
        int option = incomeTypesSelectWidget.selectedOption();
        if (option == 0) {
            return null;
        }
        return listIncomeTypes.get(option - 1);
    }

    @Override
    public String headline() {
        return "REGISTER AN INCOME";
    }
}
