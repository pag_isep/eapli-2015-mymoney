/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.framework.domain.Money;
import eapli.mymoney.application.RegisterInitialBalanceController;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author arocha
 */
public class RegisterInitialBalanceUI extends BaseUI {

    RegisterInitialBalanceController controller = new RegisterInitialBalanceController();

    @Override
    public boolean doShow() {
        Calendar date = Console.readCalendar("Reference Date (dd-MM-yyyy):");
        double initial = Console.readDouble("Initial value:");
        Money howMuch = Money.euros(initial);
        controller.registerInitialBalance(date, howMuch);

        System.out.println("\nInitial Balance recorded!");

        return true;
    }

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    public String headline() {
        return "REGISTER INITIAL BALANCE";
    }
}
