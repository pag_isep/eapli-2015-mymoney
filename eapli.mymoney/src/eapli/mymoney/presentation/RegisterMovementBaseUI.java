/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.domain.Money;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author Paulo Gandra Sousa
 */
public abstract class RegisterMovementBaseUI extends BaseUI {

    protected String what;
    protected Calendar date;
    protected Money amount;

    protected void readGeneralData() {
        what = Console.readLine("What:");
        date = Console.readCalendar("When (dd-MM-yyyy):");
        double value = Console.readDouble("How much:");
        amount = Money.euros(value);
    }
}
