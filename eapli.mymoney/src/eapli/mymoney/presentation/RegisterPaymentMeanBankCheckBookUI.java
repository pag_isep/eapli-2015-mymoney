/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.mymoney.application.RegisterPaymentMeanController;
import eapli.util.Console;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public class RegisterPaymentMeanBankCheckBookUI extends BaseUI {
    // TODO this seems like a too specialized UI
    // shouldn't we have a general UI for payment means and inside that UI
    // the user can select/choose which type to create?

    final RegisterPaymentMeanController controller = new RegisterPaymentMeanController();

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    protected boolean doShow() {
        //FIXME accept valid entries only
        final String chequeBookName = Console.readLine("How do you call your cheque account:");
        final String bankName = Console.readLine("Bank Name? » ");
        final String accountNumber = Console.readLine("Account Number? » ");

        controller.registerBankCheckBook(chequeBookName, bankName, accountNumber);

        return true;
    }

    @Override
    public String headline() {
        return "Register Payment Mean Bank Check Book";
    }

}
