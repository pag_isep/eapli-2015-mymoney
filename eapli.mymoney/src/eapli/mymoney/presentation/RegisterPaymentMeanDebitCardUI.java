/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.mymoney.application.RegisterPaymentMeanController;
import eapli.util.Console;
import java.util.Calendar;

/**
 *
 * @author Jorge Santos <ajs@isep.ipp.pt>
 */
public class RegisterPaymentMeanDebitCardUI extends BaseUI {

    private String bankName;
    private Calendar validity;
    private String accountNumber;

    final RegisterPaymentMeanController controller = new RegisterPaymentMeanController();

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    protected boolean doShow() {
        //FIXME accept valid entries only
        bankName = Console.readLine("Bank Name? » ");
        validity = Console.readCalendar("Validity? » ");
        accountNumber = Console.readLine("Account Number? » ");

        controller.registerDebitCard(bankName, validity, accountNumber);

        return true;
    }

    @Override
    public String headline() {
        return "Register Payment Mean Debit Card";
    }

    //    private void readCardData() {
//        cardName = Console.readLine("How do you call your card:");
//        bank = Console.readLine("Bank:");
//        number = Console.readLine("Card number:");
//        name = Console.readLine("Name on card:");
//        validUntil = Console.readCalendar("Valid (dd-MM-yyyy):");
//    }
}
