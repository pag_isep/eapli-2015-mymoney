/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.framework.domain.Money;
import eapli.mymoney.application.RegisterSavingGoalController;
import eapli.util.Console;

/**
 *
 * @author losa
 */
public class RegisterSavingGoalUI extends BaseUI {

    private final RegisterSavingGoalController controller = new RegisterSavingGoalController();

    @Override
    protected Controller controller() {
        return controller;
    }

    @Override
    public boolean doShow() {
        String targetDescription = Console.readLine("Description of new Target");
        double value = Console.readDouble("Target Ammount");
        Money targetAmount = Money.euros(value);

        controller.registerSavingGoal(targetDescription, targetAmount);

        System.out.println("\nSaving Goal registered.");

        return true;
    }

    @Override
    public String headline() {
        return "REGISTER NEW TARGET SAVING";
    }
}
