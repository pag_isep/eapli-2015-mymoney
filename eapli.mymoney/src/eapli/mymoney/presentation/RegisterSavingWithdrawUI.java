/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation;

import eapli.framework.Controller;
import eapli.framework.presentation.SelectWidget;
import eapli.mymoney.application.RegisterSavingWithdrawController;
import eapli.mymoney.domain.savings.SavingGoal;
import eapli.mymoney.presentation.visitors.SavingGoalListVisitor;
import java.util.List;

/**
 *
 * @author AJS
 */
public class RegisterSavingWithdrawUI extends RegisterMovementBaseUI {

    private SelectWidget<SavingGoal> widget;
    RegisterSavingWithdrawController controller = new RegisterSavingWithdrawController();

    @Override
    public String headline() {
        return "REGISTER A SAVING WITHDRAW";
    }

    @Override
    public boolean doShow() {
        // FIX this code is duplicated with RegisterSavingDepositUI
        SavingGoal savingGoal = readSavingGoal();
        readGeneralData();

        controller.registerSavingWithdraw(savingGoal, date, amount, what);
        System.out.println("\nSaving Withdraw recorded!");
        return true;
    }

    @Override
    protected Controller controller() {
        return controller;
    }

    // FIX this code is duplicated with RegisterSavingDepositUI
    private SavingGoal readSavingGoal() {
        System.out.println("-- SAVING GOAL --");
        List<SavingGoal> listSavingGoal = controller.getSavingGoals();

        widget = new SelectWidget<SavingGoal>(listSavingGoal, new SavingGoalListVisitor());
        widget.show();
        int option = widget.selectedOption();

        SavingGoal savingGoal = listSavingGoal.get(option - 1);
        return savingGoal;
    }
}
