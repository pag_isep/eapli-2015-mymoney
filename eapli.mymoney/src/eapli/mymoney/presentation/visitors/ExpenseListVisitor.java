/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation.visitors;

import eapli.framework.visitor.Visitor;
import eapli.mymoney.domain.movements.Expense;
import eapli.util.DateTime;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseListVisitor implements Visitor<Expense> {

    @Override
    public void visit(Expense visited) {
        System.out.print(DateTime.format(visited.occurredOn()) + " ");
        System.out.print(visited.amount() + " ");
        System.out.println(visited.description());
    }

    @Override
    public void beforeVisiting(Expense visited) {
        // nothing to do
    }

    @Override
    public void afterVisiting(Expense visited) {
        // nothing to do
    }
}
