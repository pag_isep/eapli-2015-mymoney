/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation.visitors;

import eapli.framework.visitor.Visitor;
import eapli.mymoney.domain.movements.ExpenseType;

/**
 * an implementation of the Visitor pattern for listing objects of type
 * ExpenseType
 *
 * @author Paulo Gandra Sousa
 */
public class ExpenseTypeListVisitor implements Visitor<ExpenseType> {

    @Override
    public void visit(ExpenseType visited) {
        System.out.println(visited.description());
    }

    @Override
    public void beforeVisiting(ExpenseType visited) {
        // nothing to do
    }

    @Override
    public void afterVisiting(ExpenseType visited) {
        // nothing to do
    }
}
