/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation.visitors;

import eapli.framework.visitor.Visitor;
import eapli.mymoney.domain.movements.Income;
import eapli.util.DateTime;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class IncomeListVisitor implements Visitor<Income> {

    public IncomeListVisitor() {
    }

    @Override
    public void visit(Income visited) {
        System.out.print(DateTime.format(visited.occurredOn()) + " ");
        System.out.print(visited.amount() + " ");
        System.out.println(visited.description());
    }

    @Override
    public void beforeVisiting(Income visited) {
        // nothing to do
    }

    @Override
    public void afterVisiting(Income visited) {
        // nothing to do
    }
}
