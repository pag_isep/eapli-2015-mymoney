/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation.visitors;

import eapli.framework.visitor.Visitor;
import eapli.mymoney.domain.paymentmeans.PaymentMean;

/**
 *
 * @author Paulo Gandra Sousa
 */
public class PaymentMeanListVisitor implements Visitor<PaymentMean> {

    public PaymentMeanListVisitor() {
    }

    @Override
    public void visit(PaymentMean visited) {
        System.out.println(visited.description());
    }

    @Override
    public void beforeVisiting(PaymentMean visited) {
        //nothing to do
    }

    @Override
    public void afterVisiting(PaymentMean visited) {
        //nothing to do
    }
}
