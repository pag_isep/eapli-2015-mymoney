/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.presentation.visitors;

import eapli.framework.visitor.Visitor;
import eapli.mymoney.domain.savings.SavingGoal;

/**
 *
 * @author AJS
 */
public class SavingGoalListVisitor implements Visitor<SavingGoal> {

    @Override
    public void visit(SavingGoal visited) {
        System.out.println(visited.getDescription());
    }

    @Override
    public void beforeVisiting(SavingGoal visited) {
        //nothing to do
    }

    @Override
    public void afterVisiting(SavingGoal visited) {
        //nothing to do
    }
}
