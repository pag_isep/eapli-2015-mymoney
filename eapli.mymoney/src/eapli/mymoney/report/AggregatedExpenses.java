/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package eapli.mymoney.report;

import eapli.framework.domain.Money;
import eapli.mymoney.domain.movements.Expense;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author nuno
 */
public class AggregatedExpenses {

    private Money sum = Money.euros(0);
    private final List<Expense> expenses = new ArrayList<Expense>();

    /**
     * Add a movement to the list
     *
     * @param expense
     * @param movement
     */
    public void aggregate(Expense expense) {
        expenses.add(expense);
        sum = sum.add(expense.amount());
    }

    /**
     * Returns all movements
     *
     * @return
     */
    public List<Expense> all() {
        return Collections.unmodifiableList(expenses);
    }

    /*
     * Returns the sum of all movements
     */
    /**
     *
     * @return
     */
    public Money getSum() {
        return sum;
    }
}
